using System.Collections.Generic;
using System.Drawing;
using NUnit.Framework;
using Vejnet;

namespace Vejnet.Tests
{
    [TestFixture]
    public class IntersectionTests
    {
        [Test]
        public void IntersectionEquals()
        {
            var inter1 = new Intersection(new Coordinate(10,5));
            var inter2 = new Intersection(new Coordinate(10,5));
            Assert.True(inter1.Equals(inter2) && inter2.Equals(inter1));
        }

        [Test]
        public void IntersectionNotEqual()
        {
            var inter1 = new Intersection(new Coordinate(5,5));
            var inter2 = new Intersection(new Coordinate(10,10));
            Assert.False(inter1.Equals(inter2) || inter2.Equals(inter1)); 
        }        

        [Test]
        public void IntersectionDefinitionEquals()
        {
            var inter1 = new Intersection(new Coordinate(20, 20));
            inter1.IntersectionDefinition.Add((new Coordinate(20, 21),0));
            inter1.IntersectionDefinition.Add((new Coordinate(21, 21),0));
            inter1.IntersectionDefinition.Add((new Coordinate(21, 20),0));

            var inter2 = new Intersection(new Coordinate(21, 21));

            Assert.True(inter1.Equals(inter2) && inter2.Equals(inter1));                        
        }

        [Test]
        public void IntersectionDefinitionNotEquals()
        {
            var inter1 = new Intersection(new Coordinate(20, 20));
            inter1.IntersectionDefinition.Add((new Coordinate(20, 21),0));
            inter1.IntersectionDefinition.Add((new Coordinate(21, 21),0));
            inter1.IntersectionDefinition.Add((new Coordinate(21, 20),0));

            var inter2 = new Intersection(new Coordinate(52, 50));
            inter2.IntersectionDefinition.Add((new Coordinate(50,50),0));
            inter2.IntersectionDefinition.Add((new Coordinate(51,51),0));

            Assert.False(inter1.Equals(inter2) || inter2.Equals(inter1));
        }

        [Test]
        public void IntersectionsAreRecognised(){
            var entranceList = new List<Entrance>();
            var exitList = new List<Exit>();
            var graph = new Graph();

            BitArray map = new BitArray(ImageManipulation.LoadImage(out entranceList, out exitList, ref graph, "../../../TestImages/Dollarsign.png"));
            
            foreach(Entrance entry in entranceList){
                graph.getEdges(map, entry);
            }
            
            List<Vertex> vertices = graph.vertexVisited;

            int intersections = 0;
            foreach(Vertex v in vertices){
                if(v is Intersection){
                    intersections++;
                }
            }

            Assert.AreEqual(3, intersections);
        }

        [Test]
        public void IntersectionsAreRecognised2(){
            var entranceList = new List<Entrance>();
            var exitList = new List<Exit>();
            var graph = new Graph();

            BitArray map = new BitArray(ImageManipulation.LoadImage(out entranceList, out exitList, ref graph, "../../../TestImages/DollarsignCounterClockWise.png"));
            
            foreach(Entrance entry in entranceList){
                graph.getEdges(map, entry);
            }
            
            List<Vertex> vertices = graph.vertexVisited;

            int intersections = 0;
            foreach(Vertex v in vertices){
                if(v is Intersection){
                    intersections++;
                }
            }

            Assert.AreEqual(3, intersections);
        }

        [Test]
        public void IntersectionsAreRecognised4(){
            var entranceList = new List<Entrance>();
            var exitList = new List<Exit>();
            var graph = new Graph();

            BitArray map = new BitArray(ImageManipulation.LoadImage(out entranceList, out exitList, ref graph, "../../../TestImages/SimpleCross.png"));
            
            foreach(Entrance entry in entranceList){
                graph.getEdges(map, entry);
            }
            
            List<Vertex> vertices = graph.vertexVisited;

            int intersections = 0;
            foreach(Vertex v in vertices){
                if(v is Intersection){
                    intersections++;
                }
            }

            Assert.AreEqual(1, intersections);
        }

        [Test]
        public void IntersectionsAreRecognised5(){
            var entranceList = new List<Entrance>();
            var exitList = new List<Exit>();
            var graph = new Graph();

            BitArray map = new BitArray(ImageManipulation.LoadImage(out entranceList, out exitList, ref graph, "../../../TestImages/ExtremeRoundAbout.png"));
            
            foreach(Entrance entry in entranceList){
                graph.getEdges(map, entry);
            }
            
            List<Vertex> vertices = graph.vertexVisited;

            int intersections = 0;
            foreach(Vertex v in vertices){
                if(v is Intersection){
                    intersections++;
                }
            }

            Assert.AreEqual(1, intersections);
        }
    }
}
