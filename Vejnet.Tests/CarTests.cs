using NUnit.Framework;
using Vejnet;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Vejnet.Tests{

    [TestFixture]
    public class CarTests{

        List<Entrance> entranceList;
        List<Exit> exitList;
        Graph graph;
        BitArray image;
        CollisionMap collisionMap;

        [SetUp]
        public void init(){

            string directory = System.IO.Directory.GetCurrentDirectory();
            entranceList = new List<Entrance>();
            exitList = new List<Exit>();
            graph = new Graph();

            image = new BitArray(ImageManipulation.LoadImage(out entranceList, out exitList, ref graph, directory + "/../../../TestImages/Antenna1.png"));

            foreach (Entrance point in entranceList)
            {
                graph.getEdges(image, point);
            }

            collisionMap = new CollisionMap(image);
        }

        [Test] 
        public void spawnCar(){
            Car testCar = new Car(entranceList[0], exitList[0], graph);

            Assert.AreEqual(testCar.State, CarState.Running);
        }

        [Test]
        public void CarCanMoveRightIfOnBlueTile(){
            Car testCar = new Car(entranceList[0], exitList[0], graph);

            testCar.Position = new Coordinate(7,23);

            testCar.CalculateWork(collisionMap);
            testCar.ApplyWork(collisionMap);

            Assert.AreEqual(new Coordinate(8,23), testCar.Position);
        }

        [Test]
        public void CarCanMoveLeftIfOnBlackTile(){
            Car testCar = new Car(entranceList[0], exitList[0], graph);

            testCar.Position = new Coordinate(7,22);

            testCar.CalculateWork(collisionMap);
            testCar.ApplyWork(collisionMap);

            Assert.AreEqual(new Coordinate(6,22), testCar.Position);
        }

        [Test]
        public void CarCanMoveUpIfOnYellowTile(){
            Car testCar = new Car(entranceList[0], exitList[0], graph);

            testCar.Position = new Coordinate(23,18);

            testCar.CalculateWork(collisionMap);
            testCar.ApplyWork(collisionMap);

            Assert.AreEqual(new Coordinate(23,17), testCar.Position);
        }

        [Test]
        public void CarCanMoveDownIfOnPinkTile(){
            Car testCar = new Car(entranceList[0], exitList[0], graph);

            testCar.Position = new Coordinate(22,18);

            testCar.CalculateWork(collisionMap);
            testCar.ApplyWork(collisionMap);

            Assert.AreEqual(new Coordinate(22,19), testCar.Position);
        }

        [Test]
        public void CarCantMoveIfCarInFront(){
            Car testCar1 = new Car(entranceList[0], exitList[0], graph);
            Car testCar2 = new Car(entranceList[0], exitList[0], graph);
            //Cars are placed on top of eachother here because they are only added to the collision map after moving,
            //therefore we first move the (to be) blocking car forward, then resolve the request. Testcar2 is now blocking the way of
            //testcar1, and testcar1 should therefore not be allowed to move.
            testCar1.Position = entranceList[0].coordinate.Clone();
            testCar2.Position = entranceList[0].coordinate.Clone();
            
            testCar1.CalculateWork(collisionMap);
            testCar2.CalculateWork(collisionMap);
            testCar1.ApplyWork(collisionMap);
            testCar2.ApplyWork(collisionMap);
            
            Assert.AreEqual(new Coordinate(1, 23), testCar1.Position);
            Assert.AreEqual(new Coordinate(0, 23), testCar2.Position);
        } 

        [Test]
        public void CarCantMoveIfCarInFront2(){
            Car testCar1 = new Car(entranceList[0], exitList[0], graph);
            Car testCar2 = new Car(entranceList[0], exitList[0], graph);

            //Placing cars on a road
            testCar1.Position = new Coordinate(2, 23);
            testCar2.Position = new Coordinate(1, 23);
            collisionMap.SetPixelCollisionMap(testCar1.Position, testCar1.CarColor);
            collisionMap.SetPixelCollisionMap(testCar2.Position, testCar2.CarColor);
            
            testCar1.CalculateWork(collisionMap);
            testCar2.CalculateWork(collisionMap);
            testCar1.ApplyWork(collisionMap);
            testCar2.ApplyWork(collisionMap);
            
            Assert.AreEqual(new Coordinate(3, 23), testCar1.Position);
            Assert.AreEqual(new Coordinate(1, 23), testCar2.Position);
        } 

        [Test]
        public void CarWillDissapearAtItsDestination(){
            Car testCar = new Car(entranceList[0], new Exit(27,14), graph);

            Coordinate lastRoadBeforeExit = new Coordinate(26,14);
            List<Coordinate> listOfLastRoadBeforeExit = new List<Coordinate>();

            listOfLastRoadBeforeExit.Add(lastRoadBeforeExit);

            testCar.Route = listOfLastRoadBeforeExit;
            testCar.Position = new Coordinate(26,14);

            testCar.CalculateWork(collisionMap);
            testCar.ApplyWork(collisionMap);

            Assert.AreEqual(CarState.Dead, testCar.State);
        }

        [Test]
        public void CarPanicsOffRoad(){
            Car testCar = new Car(entranceList[0], exitList[0], graph);

            testCar.Position = new Coordinate(1, 24); //About to enter roundabout
            collisionMap.SetPixelCollisionMap(testCar.Position, testCar.CarColor);

            Assert.Throws<CarCannotMoveException>(
                () => { testCar.CalculateWork(collisionMap); });

            testCar.ApplyWork(collisionMap);

            Assert.AreEqual(new Coordinate(1, 24), testCar.Position);
        }
                    
    }
}