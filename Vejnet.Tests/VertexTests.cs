using NUnit.Framework;
using Vejnet;

namespace Vejnet.Tests
{
    public class VertexTests
    {
        [Test]
        public void EqualsTest()
        {
            var vert1 = new Vertex(10,5);
            var vert2 = new Vertex(10,5);

            Assert.True(vert1.Equals(vert2) && vert2.Equals(vert1));
        }

        [Test]
        public void EqualsFail()
        {
            var vert1 = new Vertex(5,6);
            var vert2 = new Vertex(10,9);

            Assert.False(vert1.Equals(vert2) || vert2.Equals(vert1));
        }
    }
}