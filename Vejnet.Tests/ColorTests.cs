using NUnit.Framework;
using Vejnet;
using System.Drawing;

namespace Vejnet.Tests
{
    public class ColorTests
    {
        [Test]
        public void WhiteIsRight()
        {
		    var white = Colors.White;
            var colorApprox = Colors.ApproximateColors(white);            
            
            Assert.True(colorApprox == white);
        }

        [Test]
        public void BlackIsBlack(){
            var blk = Colors.Black;
            var colorApprox = Colors.ApproximateColors(blk);
            
            Assert.True(colorApprox == blk);
        }

        [Test]
        public void GreenIsClean()
        {
            var green = Colors.Green;
            var colorApprox = Colors.ApproximateColors(green);

            Assert.True(colorApprox == green);                        
        }

        [Test]
        public void PinkIsPink(){
            var pink = Colors.Pink;
            var colorApprox = Colors.ApproximateColors(pink);
            
            Assert.True(colorApprox == pink);
        }


        [Test]
        public void ApprRed(){
            var red = Color.FromArgb(255,255,0,0).ToArgb();
            var colorApprox = Colors.ApproximateColors(red);
            
            Assert.True(colorApprox == Colors.Red);
        }

        [Test]
        public void ApprGreen(){
            var green = Color.FromArgb(255,0,255,0).ToArgb();
            var colorApprox = Colors.ApproximateColors(green);
            
            Assert.True(colorApprox == Colors.Green);
        }

        [Test]
        public void ApprBlue(){
            var blue = Color.FromArgb(255,0,0,255).ToArgb();
            var colorApprox = Colors.ApproximateColors(blue);
            
            Assert.True(colorApprox == Colors.Blue);
        }

        [Test]
        public void BetweenRedAndBlue(){
            var violet = Color.FromArgb(255,255,0,255).ToArgb();
            var colorApprox = Colors.ApproximateColors(violet);
            
            Assert.True(colorApprox == Colors.Pink);
        }

        [Test]
        public void BetweenGreenAndBlue()
        {
            var cyan = Color.FromArgb(255,0,255,255).ToArgb();
            var colorApprox = Colors.ApproximateColors(cyan);

            Assert.True(colorApprox == Colors.Blue);
        }

        [Test]
        public void LimeIsGreen()
        {
            var lime = Color.FromArgb(255, 0, 255, 114).ToArgb();
            var colorApprox = Colors.ApproximateColors(lime);

            Assert.True(colorApprox == Colors.Green);            
        }        
    }
}
