using NUnit.Framework;
using System.Collections.Generic;
using System.Drawing;
using Vejnet;

namespace Vejnet.Tests
{
    public class GraphTests
    {
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Tests a small and simple graph with only one edge
        /// </summary>
        [Test]
        public void SimplestGraphAndPath()
        {
            Entrance entrance = new Entrance(0,0);
            Exit exit = new Exit(1,0);
            Edge edge = new Edge(1, exit);
            entrance.Edges.Add(new Edge(1, exit));
            entrance.Edges[0].Start = new Coordinate(1,0);       
		    Graph graph = new Graph();
            List<Coordinate> route = graph.GetRoute(entrance,exit);

            Assert.True(route.Count == 1);
            route.Reverse();
            Assert.True(route[0].Equals(exit.coordinate));
        }

        /// <summary>
        /// Tests a little graph with one vertex (Not an intersection), but works similar
        /// </summary>
        [Test]
        public void OneIntersection()
        {
            Entrance entrance = new Entrance(0,0);
            Vertex vert = new Vertex(0,2);            
            Exit exit = new Exit(0,4);
            entrance.Edges.Add(new Edge(2, vert));
            entrance.Edges[0].Start = new Coordinate(0,1);   
            vert.Edges.Add(new Edge(2, exit));  
            vert.Edges[0].Start = new Coordinate(0,3);   


		    Graph graph = new Graph();
            List<Coordinate> route = graph.GetRoute(entrance,exit);

            Assert.True(route.Count == 2);
            Assert.True(route[0].Equals(new Coordinate(0,1)) && route[1].Equals(new Coordinate(0,3)));
        }

        /// <summary>
        /// Tests the functionality, if one intersection has two seperate roads to the same
        /// </summary>
        [Test]
        public void TwoPossibleRoads()
        {
            Entrance entrance = new Entrance(0,0);
            Intersection inter1 = new Intersection(new Coordinate(1,1));
            Intersection inter2 = new Intersection(new Coordinate(3,3));
            Exit exit = new Exit(3,4);   
            entrance.Edges.Add(new Edge(1, inter1) {Start = new Coordinate(1,0)} );
            inter1.Edges.Add(new Edge(5, inter2) {Start = new Coordinate(2,1)} );
            inter1.Edges.Add(new Edge(3, inter2) {Start = new Coordinate(1,2)} );
            inter2.Edges.Add(new Edge(1, exit) {Start = new Coordinate(3,4)} );

		    Graph graph = new Graph();
            List<Coordinate> route = graph.GetRoute(entrance,exit);

            Assert.True(route.Count == 3);
            route.Reverse();
            Assert.True(route[0].Equals(exit.coordinate));
        }

        [Test]
        public void TestGetIntersection1()
        {
            var directory = System.IO.Directory.GetCurrentDirectory();
            Bitmap image1 = new Bitmap(Image.FromFile(directory + "/../../../TestImages/SimpleCross.png"));
            Colors.CleanImage(image1);
            BitArray image = new BitArray(image1);

            Entrance entra1 = new Entrance(0,3);

            Graph graph = new Graph();
            graph.getEdges(image, entra1);

            Assert.True(entra1.Edges.Count == 1);
            Assert.True(entra1.Edges[0].Distance == 3);
            var dest = entra1.Edges[0].Destination;

            Assert.True(dest is Intersection);
            Assert.True(dest.Edges.Count == 2);
            Assert.True(dest.Edges[0].Destination is Exit);
            Assert.True(dest.Edges[1].Destination is Exit);
        }


        [Test]
        public void TestGetIntersection2()
        {
            var directory = System.IO.Directory.GetCurrentDirectory();
            Bitmap image1 = new Bitmap(Image.FromFile(directory + "/../../../TestImages/Dollarsign.png"));
            Colors.CleanImage(image1);
            BitArray image = new BitArray(image1);

            Entrance entra1 = new Entrance(9,0);
            Entrance entra2 = new Entrance(0,9);

            Graph graph = new Graph();
            graph.getEdges(image, entra1);

            Assert.True(entra1.Edges.Count == 1);
            Assert.True(entra1.Edges[0].Distance == 3);
            var dest = entra1.Edges[0].Destination;

            Assert.True(dest is Intersection);
            Assert.True(dest.Edges.Count == 2);
            
            if (dest.Edges[0].Destination is Intersection){
                Assert.True(dest.Edges[1].Destination is Exit);
                Assert.True(dest.Edges[0].Destination is Intersection);
                Assert.True(dest.Edges[0].Destination.Edges.Count == 4);
            }
            else{
                Assert.True(dest.Edges[0].Destination is Exit);
                Assert.True(dest.Edges[1].Destination is Intersection);
                Assert.True(dest.Edges[1].Destination.Edges.Count == 4);
            }

            graph.getEdges(image,entra2);
            Assert.True(entra2.Edges.Count == 1);
            Assert.True(entra2.Edges[0].Distance == 3);
            var dest1 = entra2.Edges[0].Destination;

            Assert.True(dest1 is Intersection);
            Assert.True(dest1.Edges.Count == 2);
            
            if (dest1.Edges[0].Destination is Intersection){
                Assert.True(dest1.Edges[1].Destination is Exit);
                Assert.True(dest1.Edges[0].Destination is Intersection);
                Assert.True(dest1.Edges[0].Destination.Edges.Count == 4);
            }
            else{
                Assert.True(dest1.Edges[0].Destination is Exit);
                Assert.True(dest1.Edges[1].Destination is Intersection);
                Assert.True(dest1.Edges[1].Destination.Edges.Count == 4);
            }
        }
    }
}
