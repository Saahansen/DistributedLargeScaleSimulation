using NUnit.Framework;
using Vejnet;

namespace Vejnet.Tests
{
    public class CoordinateTests
    {
        [Test]
        public void EqualsTest1()
        {
            var coor1 = new Coordinate(10,5);
            var coor2 = new Coordinate(10,5);
            Assert.True(coor1.Equals(coor2) && coor2.Equals(coor1));                        
        }

        [Test]
        public void EqualsTest2()
        {
            var coor1 = new Coordinate(10,5);
            var coor2 = new Coordinate(11,5);
            Assert.False(coor1.Equals(coor2) || coor2.Equals(coor1));                     
        }
    }        
}