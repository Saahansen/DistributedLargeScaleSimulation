namespace Vejnet
{
    /// <summary>
    /// This is the representation of an Exit where cars will have to drive to.
    /// </summary>
    [System.Serializable]
    public class Exit : Vertex
    {
        public Exit(int x, int y) : base(x, y) { }

        public override string ToString()
            => $"Exit: {coordinate.x}, {coordinate.y}";
    }
}