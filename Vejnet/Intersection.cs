using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System;
using Framework;

namespace Vejnet
{
    [Serializable]
    public class Intersection : Vertex
    {
        /// <summary>
        /// List of coordinates and their underlying colours for a intersection
        /// </summary>
        public List<(Coordinate coor, int color)> IntersectionDefinition = new List<(Coordinate, int)>();

        /// <summary>
        /// Keeps all of the entrances that leads to the intersection
        /// </summary>
        /// <typeparam name="Coordinate"></typeparam>
        /// <returns></returns>
        public List<Coordinate> EntranceCoordinates = new List<Coordinate>();


        /// <summary>
        /// List of borders to other intersections
        /// </summary>
        public List<IBoundary> bounds = new List<IBoundary>();


        /// <summary>
        /// Collection of pixels that can be seen from the intersection
        /// This includes any entrances and their roads to the intersection, roads to exits and regular roads to borders
        /// </summary>
        public List<(Coordinate coor, int color)> pixels = new List<(Coordinate, int)>();


        /// <summary>
        /// An array of each direction enum value
        /// </summary>
        Array directions = Enum.GetValues(typeof(DirDef.Direction));


        /// <summary>
        /// Generate an intersection from any coordinate that is part of the intersection.
        /// </summary>
        public Intersection(Coordinate coordinate) : base(coordinate.x, coordinate.y)
        {
            IntersectionDefinition.Add((coordinate, 0));
        }

        public override int GetHashCode()
        {
            int hash = IntersectionDefinition.GetHashCode() * 7;
            return hash;
        }


        /// <summary>
        /// Determines whether two intersections are the same
        /// </summary>
        /// <param name="obj">Intersection to be compared</param>
        /// <returns>True if the intersections share a coordinate. Flase otherwise</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (!(obj is Intersection))
                return false;

            Intersection Snew = (Intersection)obj;
            if (Snew.IntersectionDefinition.Any(x => x.Item1.Equals(IntersectionDefinition[0].Item1)) || IntersectionDefinition.Any(x => x.Item1.Equals(Snew.IntersectionDefinition[0].Item1)))
                return true;
            else return false;
        }


        /// <summary>
        /// Finds the coordinate to where the care wants to move in the intersection.
        /// </summary>
        /// <param name="carPosition">Coordinate in intersection</param>
        /// <returns>Next coordinate in intersection</returns>
        public Coordinate GetNextPositionInIntersection(Coordinate carPosition)
        {
            if (IntersectionDefinition.First(x => x.Item1.Equals(carPosition)).Item2 == Colors.Black) //Black left
                return new Coordinate(carPosition.x - 1, carPosition.y);
            else if (IntersectionDefinition.First(x => x.Item1.Equals(carPosition)).Item2 == Colors.Blue) // Blue right
                return new Coordinate(carPosition.x + 1, carPosition.y);
            else if (IntersectionDefinition.First(x => x.Item1.Equals(carPosition)).Item2 == Colors.Pink) // Pink down
                return new Coordinate(carPosition.x, carPosition.y + 1);
            else if (IntersectionDefinition.First(x => x.Item1.Equals(carPosition)).Item2 == Colors.Yellow) // Yellow up
                return new Coordinate(carPosition.x, carPosition.y - 1);
            else return null;
        }


        /// <summary>
        /// Get the length of a given route through an intersection
        /// /// </summary>
        /// <param name="start">Entrance vertex for intersection</param> 
        /// <param name="exitCoordinate">The first coordinate of road of the exit</param>
        /// <returns>Length of a given route through an intersection</returns>
        internal int GetLength(Vertex start, Coordinate exitCoordinate)
        {
            int count = 1;
            Coordinate pos = start.coordinate.Clone();

            //While the exit sought after is not available
            while (!((pos.x == exitCoordinate.x && (pos.y + 1 == exitCoordinate.y || pos.y - 1 == exitCoordinate.y))
                || (pos.y == exitCoordinate.y && (pos.x + 1 == exitCoordinate.x || pos.x - 1 == exitCoordinate.x))))
            {
                pos = GetNextPositionInIntersection(pos);
                count++;
            }

            return count;
        }


        /// <summary>
        /// Returns the index of (one of the) the topmost pixel in an intersection
        /// </summary>
        /// <returns>The index of the topmost pixel in intersection</returns>
        private int FindTopMost()
        {
            (Coordinate coor, int color) topmost = IntersectionDefinition[0];

            foreach (var coorDef in IntersectionDefinition)
            {
                if (coorDef.coor.y < topmost.coor.y)
                    topmost = coorDef;
            }

            return IntersectionDefinition.IndexOf(topmost);
        }


        /// <summary>
        /// Gets an IntersectionDefinition pixel based on the specific Coordinate
        /// </summary>
        /// <param name="direct">The direction that should be looked in</param>
        /// <param name="index">The index of the pixel in the intersection</param>
        /// <returns>A coordinate and colour tuple</returns>
        private (Coordinate coor, int color) GetDefinition(DirDef.Direction direct, int index)
        {
            switch (direct)
            {
                case DirDef.Direction.Left:
                    return (IntersectionDefinition.FirstOrDefault(x => x.coor.Equals(new Coordinate(IntersectionDefinition[index].coor.x - 1, IntersectionDefinition[index].coor.y))));
                case DirDef.Direction.Right:
                    return (IntersectionDefinition.FirstOrDefault(x => x.coor.Equals(new Coordinate(IntersectionDefinition[index].coor.x + 1, IntersectionDefinition[index].coor.y))));
                case DirDef.Direction.Up:
                    return (IntersectionDefinition.FirstOrDefault(x => x.coor.Equals(new Coordinate(IntersectionDefinition[index].coor.x, IntersectionDefinition[index].coor.y - 1))));
                default:
                    return (IntersectionDefinition.FirstOrDefault(x => x.coor.Equals(new Coordinate(IntersectionDefinition[index].coor.x, IntersectionDefinition[index].coor.y + 1))));
            }
        }


        /// <summary>
        /// Paints the intersection pixel based on the pixels around the current IntersectionDefinition pixel
        /// </summary>
        /// <param name="dir">The current direction of the inspection</param>
        /// <param name="index">The index of the pixel in the intersection</param>
        private void PaintCurrentPixel(DirDef.Direction dir, int index)
        {
            //If the previous block is pointing towards the direction you are viewing and there is a free spot, set the direction towards the free spot (Retroactive)
            if (CheckDirection(DirDef.ReverseDirection(dir), index, Colors.ColorDirection(dir)) && PartOfIntersection(dir, index))
            {
                SetIntersectionDirection(dir, index);
            }

            //If the next spot is pointing in the direction you are viewing, point the same way (Future)
            else if (CheckDirection(dir, index, Colors.ColorDirection(dir)))
                SetIntersectionDirection(dir, index);

            //For '┴' shaped corners
            else if (CheckDirection(DirDef.TurnCounterClockWise(dir), index, Colors.ColorDirection(DirDef.TurnClockWise(dir))) && PartOfIntersection(dir, index))
                SetIntersectionDirection(dir, index);

            //For 'T' shaped corners
            else if (CheckDirection(DirDef.TurnClockWise(dir), index, Colors.ColorDirection(DirDef.TurnCounterClockWise(dir))) && PartOfIntersection(dir, index))
                SetIntersectionDirection(dir, index);

            //Special Case for 2x1 intersections
            else if (CheckDirection(dir, index, Colors.ColorDirection(DirDef.ReverseDirection(dir))) && !PartOfIntersection(DirDef.ReverseDirection(dir), index) && !PartOfIntersection(DirDef.TurnCounterClockWise(dir), index) && !PartOfIntersection(DirDef.TurnClockWise(dir), index))
                SetIntersectionDirection(dir, index);
        }


        /// <summary>
        /// Checks to see if the pixel in a certain direction is part of the Intersection, and also if that pixel is of a certain color
        /// </summary>
        /// <param name="dir">The direction of the inspection</param>
        /// <param name="index">The current IntersectionDefinition pixel index</param>
        /// <param name="color">The color, the current IntersectionDefinition pixel is compared against</param>
        /// <returns>True if the direction looked in is a part of the intersection and it is the expected colour. False otherwise.</returns>
        private bool CheckDirection(DirDef.Direction dir, int index, int color)
            => PartOfIntersection(dir, index) && GetDefinition(dir, index).color == color;


        /// <summary>
        /// Recursively checks all the pixels around an Intersection pixel, and determines which pixel to colour.
        /// </summary>
        /// <param name="index">The index of the current IntersectionDefinition pixel</param>
        private void DirectNextPixel(int index)
        {
            if (PartOfIntersection(DirDef.Direction.Left, index) && PartOfIntersection(DirDef.Direction.Right, index) && (PartOfIntersection(DirDef.Direction.Up, index) || PartOfIntersection(DirDef.Direction.Down, index)))
                directions = new DirDef.Direction[] { DirDef.Direction.Left, DirDef.Direction.Right, DirDef.Direction.Up, DirDef.Direction.Down };
            else if (PartOfIntersection(DirDef.Direction.Up, index) && PartOfIntersection(DirDef.Direction.Down, index) && (PartOfIntersection(DirDef.Direction.Left, index) || PartOfIntersection(DirDef.Direction.Right, index)))
                directions = new DirDef.Direction[] { DirDef.Direction.Up, DirDef.Direction.Down, DirDef.Direction.Left, DirDef.Direction.Right };


            foreach (DirDef.Direction dir in directions)
            {
                if (PartOfIntersection(dir, index))
                {
                    if (IntersectionDefinition[index].color == 0)
                    {
                        PaintCurrentPixel(dir, index);
                        DirectNextPixel(IntersectionDefinition.IndexOf(GetDefinition(dir, index)));
                    }
                }
            }
        }


        /// <summary>
        /// Initialises the colourisation of an intersection
        /// </summary>
        public void ColorIntersectionDefinition()
        {
            //Ensures that inspection starts from one of the topmost pixels of the roundabout
            int topmost = FindTopMost();

            //Find the right way to set the starting direction. The inner two cases are special cases for 2x1 intersections
            if (!PartOfIntersection(DirDef.Direction.Left, topmost))
            {
                if (!PartOfIntersection(DirDef.Direction.Down, topmost))
                {
                    SetIntersectionDirection(DirDef.Direction.Right, topmost);
                }
                else
                    SetIntersectionDirection(DirDef.Direction.Down, topmost);
            }
            else
                SetIntersectionDirection(DirDef.Direction.Left, topmost);

            //Look at all four sides of the topmost square
            foreach (DirDef.Direction dir in directions)
            {
                if (PartOfIntersection(dir, topmost))
                    DirectNextPixel(IntersectionDefinition.IndexOf(GetDefinition(dir, topmost)));
            }
        }


        /// <summary>
        /// Sets the color of the intersection squares based on the DIRECTION. This is to avoid confusion for the colors and use Directions instead
        /// </summary>
        /// <param name="dir">Is the direction the square should move the Car</param>
        /// <param name="i">Is the current IntersectionDefinition Square</param>
        private void SetIntersectionDirection(DirDef.Direction dir, int i)
        {
            switch (dir)
            {
                case DirDef.Direction.Up:
                    IntersectionDefinition[i] = (IntersectionDefinition[i].coor, Colors.Yellow); break; //UP
                case DirDef.Direction.Left:
                    IntersectionDefinition[i] = (IntersectionDefinition[i].coor, Colors.Black); break; //LEFT
                case DirDef.Direction.Down:
                    IntersectionDefinition[i] = (IntersectionDefinition[i].coor, Colors.Pink); break; //DOWN
                default:
                    IntersectionDefinition[i] = (IntersectionDefinition[i].coor, Colors.Blue); break; //RIGHT
            }
        }


        /// <summary>
        /// Checks if the square to the specified DIRECTION is a part of the Intersection.
        /// </summary>
        /// <param name="pos">Is the direction the scope is looking at</param>
        /// <param name="index">Is the index of the current IntersectionDefinition Square</param>
        /// <returns>TRUE if the square to the POS is in the IntersectionDefinition, FALSE otherwise</returns>
        private bool PartOfIntersection(DirDef.Direction pos, int index)
        {
            switch (pos)
            {
                case DirDef.Direction.Up:
                    return IntersectionDefinition.Any(x => x.coor.Equals(new Coordinate(IntersectionDefinition[index].coor.x, IntersectionDefinition[index].coor.y - 1)));
                case DirDef.Direction.Down:
                    return IntersectionDefinition.Any(x => x.coor.Equals(new Coordinate(IntersectionDefinition[index].coor.x, IntersectionDefinition[index].coor.y + 1)));
                case DirDef.Direction.Left:
                    return IntersectionDefinition.Any(x => x.coor.Equals(new Coordinate(IntersectionDefinition[index].coor.x - 1, IntersectionDefinition[index].coor.y)));
                default:
                    return IntersectionDefinition.Any(x => x.coor.Equals(new Coordinate(IntersectionDefinition[index].coor.x + 1, IntersectionDefinition[index].coor.y)));
            }
        }
    }
}