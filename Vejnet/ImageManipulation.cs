using System.Collections.Generic;
using System.Drawing;
using ImageMagick;
using System.IO;

namespace Vejnet
{
    /// <summary>
    /// Class used to handle input and output pictures/animations from simulation
    /// </summary>
    [System.Serializable]
    public static class ImageManipulation
    {
        /// <summary>
        /// Generates Bitmap and graph from a picture
        /// </summary>
        /// <param name="entrances">Empty list of entrances to be filled in</param>
        /// <param name="exits">Empty list of exits to be filled in</param>
        /// <param name="currentGraph">Empty graph to be filled in</param>
        /// <param name="inputFile">Input file link</param>
        /// <returns>Picture loaded as Bitmap</returns>
        public static Bitmap LoadImage(out List<Entrance> entrances, out List<Exit> exits, ref Graph currentGraph, string inputFile)
        {
            using (var image = new Bitmap(Image.FromFile(inputFile)))
            {
                entrances = new List<Entrance>();
                exits = new List<Exit>();

                Size imageSize = image.Size;
                for (int x = 0; x < imageSize.Width; x++)
                {
                    for (int y = 0; y < imageSize.Height; y++)
                    {
                        int PixelColor = Colors.ApproximateColors(image.GetPixel(x, y).ToArgb());
                        image.SetPixel(x, y, Color.FromArgb(PixelColor));

                        if (PixelColor == Colors.Green)
                        {
                            var entra = new Entrance(x, y);
                            entrances.Add(entra);
                        }
                        else if (PixelColor == Colors.Red)
                        {
                            var exit = new Exit(x, y);
                            exits.Add(exit);
                            if (!currentGraph.vertexVisited.Contains(exit))
                                currentGraph.vertexVisited.Add(exit);
                        }

                    }
                }

                return new Bitmap(image);
            }
        }

        /// <summary>
        /// Converts an image to a byte-array
        /// </summary>
        /// <param name="img">Image to be converted</param>
        /// <returns>Byte representation of picture</returns>
        public static byte[] ImageToByte(Image img)
        {
            using (var stream = new MemoryStream())
            {
                //Saves the image to the memorystream as a Bitmap
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);

                return stream.ToArray();
            }
        }

        /// <summary>
        /// Generates a gif from and appends it existing if necessary and saves to disk as 'City_Anim.gif'
        /// </summary>
        /// <param name="gifList">List of images to generate gif from</param>
        /// <param name="loadPreviousGif">Boolean value to indicate whether generated gif should be appended to existing gif</param>
        public static void CreateGif(List<Image> gifList, bool loadPreviousGif)
        {
            MagickImageCollection coll = new MagickImageCollection();

            if (loadPreviousGif)
                coll.Read("City_Anim.gif");

            for (int i = 0; i < gifList.Count; i++)
            {
                MagickImage magickImage = new MagickImage(ImageManipulation.ImageToByte(gifList[i]));
                coll.Add(magickImage);
                coll[coll.Count - 1].AnimationDelay = 15;
            }

            coll.Write("City_Anim.gif");
        }
    }
}