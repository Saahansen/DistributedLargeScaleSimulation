using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Vejnet
{
    [System.Serializable]
    public class Vertex
    {
        public Coordinate coordinate;

        /// <summary>
        /// Heuristic Score for A*
        /// </summary>
        public double hscore;

        /// <summary>
        /// The distance from previous to this node
        /// </summary>
        public double gscore;

        public Vertex cameFrom;

        public List<Edge> Edges = new List<Edge>();
        public Vertex(int x, int y){
            coordinate = new Coordinate(x, y);
        }


        /// <summary>
        /// Recursively generates an edge by walking it on the ColouredMap until it reaches its destination
        /// </summary>
        /// <param name="img">Coloured map to inspect</param>
        /// <param name="current">The current position</param>
        /// <param name="length">The current lenght of the edge</param>
        /// <param name="graph">The graph representing the map</param>
        /// <returns>New edge with length and destination set. The origin should be set from the call place.</returns>
        public Edge getEdge(BitArray img, Coordinate current, int length, Graph graph){
            Coordinate nextRoad = new Coordinate(current.x,current.y);   

            if (img.GetPixel(nextRoad.x, nextRoad.y) == Colors.Black) //Black left
                nextRoad.x = nextRoad.x - 1;
            else if(img.GetPixel(nextRoad.x, nextRoad.y) == Colors.Blue) // Blue right
                nextRoad.x = nextRoad.x + 1;
            else if (img.GetPixel(nextRoad.x, nextRoad.y) == Colors.Pink) // Pink down
                nextRoad.y = nextRoad.y + 1;
            else if(img.GetPixel(nextRoad.x, nextRoad.y) == Colors.Yellow) // Yellow up
                nextRoad.y = nextRoad.y - 1;
            
            //If an intersection is reached the edge is complete
            else if(img.GetPixel(current.x,current.y) == Colors.Orange)
            {
                Intersection inter = new Intersection(current);
                if(graph.vertexVisited.Any(x => x.Equals(inter)))
                    return new Edge(length, graph.vertexVisited.First(x => x.Equals(inter)));
                return new Edge(length, inter);
            }
            //If an exit is reached the edge is complete
            else if (img.GetPixel(current.x,current.y) == Colors.Red){
                Exit exit = new Exit(current.x, current.y);
                if(graph.vertexVisited.Any(x => x.Equals(exit)))
                    return new Edge(length, graph.vertexVisited.First(x => x.Equals(exit)));
                return new Edge(length,exit);
            }
            else
                throw new NoPathExistsException("The getEdge method tried to move to a coordinate that was not black, blue," +
                                                "pink, yellow, orange or red");

            return getEdge(img, nextRoad,length + 1, graph); 
        }

        /// <summary>
        /// Determines whether two verticies are the same
        /// </summary>
        /// <param name="obj">Vertex to be compared</param>
        /// <returns>True if both verticies have the same coordinate. False otherwise</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Vertex))
            {
                return false;
            }
            
            return this.coordinate.Equals(((Vertex)obj).coordinate);
        }
        
        // override object.GetHashCode
        public override int GetHashCode()
        {
            int value = this.coordinate.GetHashCode() * 7;
            return value;
        }        

        public override string ToString()
            => $"{coordinate.x}, {coordinate.y}";
    }
}