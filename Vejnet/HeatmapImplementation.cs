using Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Drawing;
using System.Linq;
using System.Drawing.Imaging;
namespace Vejnet
{
    public class HeatmapImplementation
    {
        private Bitmap _colouredMap;
        private Bitmap _heatMap;

        /// <summary>
        /// Takes an input picture and create an empty map with same measure
        /// </summary>
        /// <param name="ColouredMap"> This is the input picture </param>
        public HeatmapImplementation(Bitmap ColouredMap)
        {
            _colouredMap = ColouredMap;
            _heatMap = new Bitmap(_colouredMap.Width, _colouredMap.Height);
        }

        /// <summary>
        /// Creates the heatmap and colors intersections and edges according to traffic. 
        /// </summary>
        /// <param name="HeatValues"> A list of roads with a number specifying how many cars was in queue on the road</param>
        public void CreateHeatmap(List<(Coordinate coordinate, int queueValue)> HeatValues)
        {
            BitArray arrayMap = new BitArray(_colouredMap);
            colourIntersectionsHeatmap(arrayMap);

            colourEdgesHeatmap(HeatValues, arrayMap);
            // Saves finished picture
            _heatMap.Save("Heatmappicture.png", ImageFormat.Png);
        }

        /// <summary>
        /// Colors all the edges of the heatmap
        /// </summary>
        /// <param name="edge"></param>
        /// <param name="queueValue"></param>
        private void colourEdgesHeatmap(List<(Coordinate coordinate, int queueValue)> HeatValues, BitArray arrayMap)
        {
            int maxvalue = HeatValues.Max(x => x.queueValue);
            // Colors edges
            foreach (var heatEdge in HeatValues)
            {
                // Determines relativ color gradiants for edges. 
                int Color = CalculateRelativQueueValues(heatEdge, maxvalue);
                ColourEdge(arrayMap, heatEdge.coordinate, Color);
            }
        }

        /// <summary>
        /// Colors the intersections of the heatmap. Checks whether the pixel is orange on the simulation map and dyes it on the heatmap. 
        /// </summary>
        /// <param name="compareMap"> The simulation map </param>
        private void colourIntersectionsHeatmap(BitArray compareMap)
        {
            for (int i = 0; i < compareMap.Width; i++)
            {
                for (int j = 0; j < compareMap.Height; j++)
                {
                    if (compareMap.GetPixel(i, j) == Colors.Orange)
                        _heatMap.SetPixel(i, j, Color.FromArgb(Colors.Orange));
                }
            }
        }

        /// <summary>
        /// Finds the color for an edge in the heatmap. 
        /// </summary>
        /// <param name="edgeInfo">A tuple of an edge with its queue value </param>
        /// <param name="max">The highest queue value </param>
        /// <returns>An interger from 1 to 5 to determine color (green, yellow, orange, red, black) </returns>
        private int CalculateRelativQueueValues((Coordinate coordinate, int queueValue) edgeInfo, int max)
        {
            double currentQueueValue, highestQueueValue, change;
            currentQueueValue = (double)edgeInfo.queueValue;
            highestQueueValue = (double)max;
            if (max == 0)
                return 1;
            change = currentQueueValue / highestQueueValue;
            if (change > 0.9)
                return 5;
            return (int)Math.Ceiling((change / 0.2) + 0.01);
        }

        /// <summary>
        /// The next coordinate to color is found, and if the next is either an exit or an intersection it returns null.
        /// A switch case controls what color the current coordinate is colored according to the heatvalue of the edge. 
        /// </summary>
        /// <param name="map"> Simulation map. </param>
        /// <param name="startCoordinate"> The current coordinate that is being colored. </param>
        /// <param name="heatvalue"> Calculated in other method. Determines color for pixel. </param>
        /// <returns> Recursive return until the end of the road is reached. </returns>
        private Edge ColourEdge(BitArray map, Coordinate startCoordinate, int heatvalue)
        {
            Coordinate nextRoad = new Coordinate(startCoordinate.x, startCoordinate.y);

            // Finds next pixel 
            if (map.GetPixel(nextRoad.x, nextRoad.y) == Colors.Black) //Black left
                nextRoad.x = nextRoad.x - 1;

            else if (map.GetPixel(nextRoad.x, nextRoad.y) == Colors.Blue) // Blue right
                nextRoad.x = nextRoad.x + 1;

            else if (map.GetPixel(nextRoad.x, nextRoad.y) == Colors.Pink) // Pink down
                nextRoad.y = nextRoad.y + 1;

            else if (map.GetPixel(nextRoad.x, nextRoad.y) == Colors.Yellow) // Yellow up
                nextRoad.y = nextRoad.y - 1;

            else if ((map.GetPixel(nextRoad.x, nextRoad.y) == Colors.Orange) || (map.GetPixel(nextRoad.x, nextRoad.y) == Colors.Red))
                return null; // edge is done

            //Array of colors used to color the heatmap
            Color[] heatColors = new Color[]
            {
                Color.FromArgb(0, 0, 0),
                Color.FromArgb(0, 255, 0),
                Color.FromArgb(255,255,0),
                Color.FromArgb(255,125,0),
                Color.FromArgb(255,0,0),
                Color.FromArgb(0,0,0),
            };

            // Colores pixel
            _heatMap.SetPixel(startCoordinate.x, startCoordinate.y, heatColors[heatvalue]);

            return ColourEdge(map, nextRoad, heatvalue); //recursive call for next coordinate on edge
        }

        /// <summary>
        /// Updates number of cars that has been stuck on a edge.
        /// If the edge does not exist in Heatvalues, it gets added with 0 as its number of stuck cars. 
        /// </summary>
        /// <param name="HeatValues"> a list of edges and integers. Represents roads and number of cars stuck on that road. </param>
        /// <param name="results"> A list of results that tells where a car has drived and whether it was stuck. </param>
        /// <returns> Updated HeatValues </returns>
        public List<(Coordinate, int)> UpdateHeatValues(List<(Coordinate coordinate, int queueValue)> HeatValues, List<CarSimulationWorkResult> results)
        {
            foreach (CarSimulationWorkResult result in results)
            {
                if (!result.CarCanMove) // Car is stuck
                {
                    if (HeatValues.Any(x => x.coordinate.Equals(result.StartCoordinate))) // Does edge exist in heatvalues
                    {
                        int index = HeatValues.FindIndex(x => x.coordinate.Equals(result.StartCoordinate));

                        // Increments number of cars stuck
                        var edgeInfo = HeatValues[index];
                        edgeInfo.queueValue++;
                        HeatValues[index] = edgeInfo;
                    }
                    else
                    {
                        HeatValues.Add((result.StartCoordinate, 1)); // Add edge to heatvalues with 1 car stuck
                    }
                }
                else if (!HeatValues.Any(x => x.coordinate.Equals(result.StartCoordinate))) // Car was not stuck, does the edge exist?
                {
                    HeatValues.Add((result.StartCoordinate, 0)); // Add the edge
                }
            }
            return HeatValues;
        }
    }

}