using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace Vejnet
{
    /// <summary>
    /// Class to load or generate cars for simulation
    /// </summary>
    public static class CarLoader
    {
        /// <summary>
        /// Load car from file
        /// File must be in the following format:
        /// start-x;start-y;end-x:end-y;optional-colour-ARGB
        /// </summary>
        /// <param name="inputFileURL">Link to file</param>
        /// <param name="entranceList">List of entrances on a map that has already been loaded</param>
        /// <param name="exitList">List of exits on a map that has already been loaded</param>
        /// <param name="graph">Graph of a map that has already been loaded</param>
        /// <returns>List of cars generated from loaded file</returns>
        public static List<Car> MakeCarsFromFile(string inputFileURL, List<Entrance> entranceList, List<Exit> exitList, Graph graph)
        {

            using (StreamReader sr = new StreamReader(inputFileURL))
            {
                var cars = new List<Car>();
                string line = sr.ReadLine();

                //Reading until end of file
                while (line != null)
                {
                    string[] strArr = line.Split(';');

                    Entrance start;
                    Exit goal;
                    try
                    {
                        //Finds entrance and exit from coordinates
                        start = entranceList.First(x => x.coordinate.Equals(new Coordinate(int.Parse(strArr[0]), int.Parse(strArr[1]))));
                        goal = exitList.First(x => x.coordinate.Equals(new Coordinate(int.Parse(strArr[2]), int.Parse(strArr[3]))));
                    }
                    catch (FormatException)
                    {
                        //Ignores line if coordinates cannot be parsed
                        line = sr.ReadLine();
                        continue;
                    }

                    //Tries to pass colour if valid
                    bool colourFound;
                    int colourCode = int.MaxValue;
                    try
                    {
                        colourCode = int.Parse(strArr[4]);
                        colourFound = true;
                    }
                    catch (Exception ex) when (ex is IndexOutOfRangeException || ex is FormatException)
                    {
                        colourFound = false;
                    }

                    //Used colour if possible otherwise lets car constructor figure it out
                    if (colourFound)
                        cars.Add(new Car(start, goal, graph, colourCode));
                    else
                        cars.Add(new Car(start, goal, graph));

                    line = sr.ReadLine();
                }

                return cars;
            }
        }

        /// <summary>
        /// Generates random colour in ARGB format 
        /// </summary>
        /// <returns>Colourcode in ARGB format with full opacity and between 50 and 200 value for red, green, and blue</returns>
        private static int randomCarColour()
        {
            Random ran = new Random();
            return Color.FromArgb(255, ran.Next(50, 200), ran.Next(50, 200), ran.Next(50, 200)).ToArgb();
        }


        /// <summary>
        /// Generates a list of cars.
        /// </summary>
        /// <param name="amount">Amount of cars to be generated</param>
        /// <param name="entranceList">List of entrances on a map that has already been loaded</param>
        /// <param name="exitList">List of exits on a map that has already been loaded</param>
        /// <param name="graph">Graph of a map that has already been loaded</param>
        /// <returns>List of cars with random entrances, destinations, and colours.</returns>
        public static List<Car> MakeCarsRandomly(int amount, List<Entrance> entranceList, List<Exit> exitList, Graph graph)
        {
            Random ran = new Random();
            var cars = new List<Car>();
            for (int i = 1; i <= amount; i++)
                cars.Add(new Car(entranceList[ran.Next(entranceList.Count)], exitList[ran.Next(exitList.Count)], graph, randomCarColour()));
            return cars;
        }
    }
}