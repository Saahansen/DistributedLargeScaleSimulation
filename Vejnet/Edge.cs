namespace Vejnet
{
    [System.Serializable]
    public class Edge
    {
        public int Distance;
        public Vertex Destination;
        private Coordinate _start;


        /// <summary>
        /// The first coordinate in the edge
        /// </summary>
        /// <value></value>
        public Coordinate Start
        {
            get { return _start; }
            set { _start = value; }
        }
        public Edge(int dist, Vertex dest)
        {
            Distance = dist;
            Destination = dest;
        }

        public override bool Equals(object obj)
        {
            if(obj == null || !((obj is Edge edgeObject) && (_start.Equals(edgeObject._start)) && (Destination.Equals(edgeObject.Destination)))){
                return false;
            }
            return true;
        }
        
        // override object.GetHashCode
        public override int GetHashCode()
        {
            return (Destination.GetHashCode() * 7) * (_start.GetHashCode() * 5);
        }

        public override string ToString()
            => $"Edge: ({Destination.coordinate.x}, {Destination.coordinate.y}) with Distance {Distance}. Starts in {Start}";
    }
}