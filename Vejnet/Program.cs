﻿using System.Collections.Generic;
using Framework;
using System.Net;
using System;

namespace Vejnet
{
    public class Program
    {
        static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                DisplayMenu Dm = new DisplayMenu();
                return 0;
            }

            ParseArgs.parseArgs(args);
            StartSimWithParsedArgs(args);
            return 0;
        }

        private static void StartSimWithParsedArgs(string[] args)
        {
            SimulationController simControl = null;
            if (ParseArgs.Role.Equals("local") || ParseArgs.Role.Equals("server"))
            {
                if(ParseArgs.File == null)
                    ParseArgs.PrintAndExit("You must specify an input map, when running locally or as a server.",0);
                
                try{
                    simControl = new SimulationController(ParseArgs.File);
                }catch(System.IO.FileNotFoundException) {
                    ParseArgs.PrintAndExit("The file specified was not valid", 0);
                }

                if(ParseArgs.AgentCreation.Equals("random"))
                    simControl.ImportCars(CarLoader.MakeCarsRandomly(ParseArgs.NumAgents,simControl.EntranceList, simControl.ExitList, simControl.Graph));
                else
                    simControl.ImportCars(CarLoader.MakeCarsFromFile(ParseArgs.AgentCreation, simControl.EntranceList, simControl.ExitList, simControl.Graph));
            }

            if (ParseArgs.Role.Equals("server"))
            {
                if(ParseArgs.NumClients == 0)
                    ParseArgs.PrintAndExit("You must set the number of initial clients",0);

                List<Task> tasks = simControl.CreateTasks();
                simControl.StartServer(ParseArgs.Port, tasks, ParseArgs.NumFrames); //Currently this also starts the sim iteself
                simControl.StartSimulation(ParseArgs.NumClients); //Starts the distributing of workload
            }
            else if (ParseArgs.Role.Equals("client"))
            {
                if(ParseArgs.IP == null)
                    ParseArgs.PrintAndExit("You must specify an IP address, when running as a client.",0);

                var ip = IPAddress.Parse(ParseArgs.IP);
                Client client = new Client(ip, ParseArgs.Port);
                client.Start();
            }
            else if (ParseArgs.Role.Equals("local"))
                simControl.RunSimulationLocally(ParseArgs.NumFrames);

            Environment.Exit(0);
        }
    }
}