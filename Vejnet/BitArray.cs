using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Vejnet
{
    /// <summary>
    /// A two-dimensional array containging ARGB values to represent a bitmap.
    /// </summary>
    [Serializable]
    public class BitArray
    {
        private List<(int PixelAmount, int Color)> colorPairs;
        private int[,] array;
        public int Height { get; }
        public int Width { get; }
        public bool isCompressed = false;

        public enum Conversion{
            Compress,
            Extract
        }


        /// <summary>
        /// Make a new BitArray from an existing one.
        /// </summary>
        /// <param name="bit">Existing BitArray</param>
        public BitArray(BitArray bit)
        {
            Height = bit.Height;
            Width = bit.Width;
            array = new int[Width, Height];
        
            for (int i = 0; i < Height; i++)
                for (int j = 0; j < Width; j++)
                    SetPixel(j, i, bit.GetPixel(j, i));
        
        }

        /// <summary>
        /// The method is used to choose if the bitArray is to be compressed or Extracted
        /// </summary>
        /// <param name="convertionWay">The enum which is used to choose between (Compress, Extracted)</param>
        public void Convert(Conversion convertionWay)
        {
            if (convertionWay.Equals(Conversion.Extract))
                ExtractBitArray();
            else
                CompressBitArray();
        }

        /// <summary>
        /// This method turns the bitArray from Int[,] to List<(Int, Int)> (Compressed)
        /// </summary>
        private void CompressBitArray()
        {
            colorPairs = new List<(int, int)>();
            int storedColor = 0;
            int storedColorAmount = 0;

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    //If the stored color is equal to the current color on the image, increment the color counter
                    if (storedColor == GetPixel(x, y))
                        storedColorAmount++;
                    else
                    {
                        colorPairs.Add((storedColorAmount, storedColor));
                        storedColor = GetPixel(x, y);
                        storedColorAmount = 1;
                    }
                }
            }

            colorPairs.Add((storedColorAmount, storedColor));
            array = new int[0, 0];
            isCompressed = true;
        }

        /// <summary>
        /// This method is used to turn the BitArray from List<(Int, Int)> to Int[,] (Extracted)
        /// </summary>
        private void ExtractBitArray()
        {
            array = new int[Width, Height];
            int y = 0;
            int x = 0;

            foreach (var keyValue in colorPairs)
            {
                for (int i = 0; i < keyValue.PixelAmount; i++, x++)
                {
                    if (x % Width == 0 && x != 0)
                    {
                        y++;
                        x = 0;
                    }
                    array[x, y] = keyValue.Color;
                }
            }
            isCompressed = false;
        }


        /// <summary>
        /// Make a BitArray from an exisiting Bitmap.
        /// </summary>
        /// <param name="bit">Existing Bitmap</param>
        public BitArray(Bitmap bit)
        {
            Height = bit.Height;
            Width = bit.Width;
            array = new int[Width, Height];

            for (int i = 0; i < Height; i++)
                for (int j = 0; j < Width; j++)
                    SetPixel(j, i, bit.GetPixel(j, i).ToArgb());
        }


        /// <summary>
        /// Creates an empty BitArray (Coloured black with 0 opacity)
        /// </summary>
        /// <param name="width">Width of the BitArray</param>
        /// <param name="height">Height of the BitArray</param>
        public BitArray(int width, int height)
        {
            Height = height;
            Width = width;
            array = new int[Width, Height];
        }


        /// <summary>
        /// Get the colour of a pixel
        /// </summary>
        /// <param name="x">x-coordinate</param>
        /// <param name="y">y-coordinate</param>
        /// <returns>The colourcode of the pixel in ARGB format</returns>
        public int GetPixel(int x, int y) => array[x, y];


        /// <summary>
        /// Set the colour of a pixel
        /// </summary>
        /// <param name="x">x-coordinate</param>
        /// <param name="y">y-coordinate</param>
        /// <param name="color">Colourcode in ARGB format</param>
        public void SetPixel(int x, int y, int color) => array[x, y] = color;


        /// <summary>
        /// Converts the BitArray to a Bitmap
        /// </summary>
        /// <returns>Bitmap representation of BitArray</returns>
        public Bitmap ToBitmap()
        {
            Bitmap bit = new Bitmap(Width, Height);
            for (int i = 0; i < Height; i++)
                for (int j = 0; j < Width; j++)
                    bit.SetPixel(j, i, Color.FromArgb(GetPixel(j, i)));
            return bit;
        }
    }
}