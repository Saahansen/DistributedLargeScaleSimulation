using System;
using System.Runtime.Serialization;

namespace Vejnet
{
    /// <summary>
    /// The car is unable to determine its next position and therefore has no way of knowing what to do
    /// </summary>
    [Serializable]
    public class CarCannotMoveException : Exception
    {
        public CarCannotMoveException()
        {
        }

        public CarCannotMoveException(string message) : base(message)
        {
        }

        public CarCannotMoveException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CarCannotMoveException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}