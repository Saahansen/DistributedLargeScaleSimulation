using System;
using System.Drawing;
using Framework;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Diagnostics;
using System.IO;

namespace Vejnet
{
    /// <summary>
    /// Class for setting up at starting the simulation
    /// </summary>
    public class SimulationController
    {
        private CollisionMap _map;
        private VejnetServer _server;
        public List<Entrance> EntranceList { get; }
        public List<Exit> ExitList { get; }
        public Graph Graph { get; }
        public List<IAgent> Cars { get; set; }


        /// <summary>
        /// Initialise SimulationController and build map and graph
        /// </summary>
        /// <param name="inputFileURL">Input file link</param>
        public SimulationController(String inputFileURL)
        {
            var _graph = new Graph();
            var _entranceList = new List<Entrance>();
            var _exitList = new List<Exit>();
            _map = new CollisionMap(new BitArray(ImageManipulation.LoadImage(out _entranceList, out _exitList, ref _graph, inputFileURL)));

            Cars = new List<IAgent>();
            Graph = _graph;
            EntranceList = _entranceList;
            ExitList = _exitList;

            //Make graph from map
            foreach (Entrance point in EntranceList)
                Graph.getEdges(_map.colouredRoad, point);
        }


        /// <summary>
        /// Starts the simulation
        /// </summary>
        internal void StartSimulation(int NumberOfClients) => _server.Start(NumberOfClients);


        /// <summary>
        /// Initialises new server
        /// </summary>
        /// <param name="port">Network port for the server to use</param>
        /// <param name="tasks">List of tasks to be solved</param>
        internal void StartServer(ushort port, List<Task> tasks, int numberOfFrames) => _server = new VejnetServer(port, tasks, _map, numberOfFrames);



        /// <summary>
        /// Adds list of cars to Cars
        /// </summary>
        /// <param name="carList">List of cars to be imported</param>
        public void ImportCars(List<Car> carList) => Cars.AddRange(Serialisation.CleanInput<Car>(carList));


        /// <summary>
        /// Creates tasks for the simulation to solve.
        /// Requires that map, graph, and cars have been initialised
        /// </summary>
        /// <returns>List of tasks</returns>
        public List<Task> CreateTasks()
        {
            List<Task> tasks = new List<Task>();
            List<Intersection> intersections = new List<Intersection>();

            //Save the intersections for later use
            foreach (var vertex in Graph.vertexVisited)
                if (vertex is Intersection)
                    intersections.Add(vertex as Intersection);

            foreach (Intersection inter in intersections)
            {
                //Add all pixels from the intersection itself
                foreach (var interDefinition in inter.IntersectionDefinition)
                    inter.pixels.Add((interDefinition.coor, Colors.Orange));

                //Get pixels from edges
                foreach (Edge edge in inter.Edges)
                {
                    GetPixelsFromEdge(edge, inter.pixels, inter.bounds);
                }
            }

            //Adds pixels from each entrance to the intersection that it leads to
            foreach (Entrance enter in EntranceList)
            {
                (enter.Edges.First().Destination as Intersection).EntranceCoordinates.Add(enter.coordinate);
                (enter.Edges.First().Destination as Intersection).pixels.Add((enter.coordinate, Colors.Green));
                int currentColor = 0;
                Coordinate start = enter.Edges.First().Start;
                addPixels(enter.Edges.First(), (enter.Edges.First().Destination as Intersection).pixels, ref currentColor, ref start, enter.Edges.First().Distance + 1);
            }

            //Call the constructor for the cMap that creates a submap and get cars that belong there
            foreach (Intersection inter in intersections)
            {
                inter.Edges = null;
                tasks.Add(new Task(new CollisionMap(inter.pixels, inter.bounds) {Intersection = inter}, getAgents(inter)));
            }

            //Compress all the bitArrays to use less space when sending
            foreach (Task task in tasks)
            {
                (task.Area as CollisionMap).colouredRoad.Convert(BitArray.Conversion.Compress);
                (task.Area as CollisionMap).collisionMap.Convert(BitArray.Conversion.Compress);
            }

            return tasks;
        }


        /// <summary>
        /// Walks an edge, defines border between intersections and draws the parts belonging to each intersection
        /// </summary>
        /// <param name="edge">The edge to be walked</param>
        /// <param name="pixels">Tuple of the pixels that represent the road</param>
        /// <param name="bounds">The list of borders for a intersection</param>
        private void GetPixelsFromEdge(Edge edge, List<(Coordinate, int)> pixels, List<IBoundary> bounds)
        {
            Coordinate currentCoordinate = new Coordinate(edge.Start.x, edge.Start.y);
            int currentColor = _map.GetPixelColouredRoad(currentCoordinate);

            //If the destination is an exit no boundary is made and the pixels are added
            if (edge.Destination is Exit)
                addPixels(edge, pixels, ref currentColor, ref currentCoordinate, edge.Distance + 1);

            //Handling edge case that only one pixel is between two intersections
            else if (edge.Distance <= 1)
            {
                var startColor = _map.GetPixelColouredRoad(edge.Start);
                pixels.Add((edge.Start, startColor));

                //Adds entrance coordinate in intersection to boundary
                Coordinate nextCoordinate = getNextPositionOnMap(edge.Start, startColor);
                pixels.Add((nextCoordinate, Colors.Orange));
                bounds.Add(new Border(edge.Start, nextCoordinate));

                //Adds the same to destination intersection
                var dest = edge.Destination as Intersection;
                dest.pixels.Add((edge.Start, startColor));
                dest.pixels.Add((nextCoordinate, Colors.Orange));
                dest.bounds.Add(new Border(edge.Start, nextCoordinate));
            }
            else //Regular case
            {
                int lengthToTravel = (edge.Distance / 2) - 1;
                addPixels(edge, pixels, ref currentColor, ref currentCoordinate, lengthToTravel);

                //Adding the end of the border to list of pixels
                Coordinate nextCoordinate = getNextPositionOnMap(currentCoordinate, currentColor);
                int nextColor = _map.GetPixelColouredRoad(nextCoordinate);
                pixels.Add((currentCoordinate, currentColor));
                pixels.Add((nextCoordinate, nextColor));
                //Generating the border
                bounds.Add(new Border(currentCoordinate, nextCoordinate));

                //Add to the destination intersection
                (edge.Destination as Intersection).bounds.Add(new Border(currentCoordinate, nextCoordinate));
                (edge.Destination as Intersection).pixels.Add((currentCoordinate, currentColor));

                addPixels(edge, (edge.Destination as Intersection).pixels, ref nextColor, ref nextCoordinate, lengthToTravel + 1 + edge.Distance % 2);
            }
        }


        /// <summary>
        /// Adds pixels from roads to list of pixels of wanted lenght
        /// </summary>
        /// <param name="edge">Edge to be walked</param>
        /// <param name="pixels">List of pixel definitions to add pixels to</param>
        /// <param name="currentColor">The color of the current position</param>
        /// <param name="currentCoordinate">The coordinate of the current position</param>
        /// <param name="length">Pixels to walk</param>
        private void addPixels(Edge edge, List<(Coordinate, int)> pixels, ref int currentColor, ref Coordinate currentCoordinate, int length)
        {
            for (int i = 1; i <= length; i++)
            {
                pixels.Add((currentCoordinate, currentColor));
                currentCoordinate = getNextPositionOnMap(currentCoordinate, currentColor);
                currentColor = _map.GetPixelColouredRoad(currentCoordinate);
            }
        }


        /// <summary>
        /// Computes the next coordinate if the coloured map is to be followed
        /// </summary>
        /// <param name="currentCoordinate">The position to generate from</param>
        /// <param name="currentColor">Colourcode in ARGB format</param>
        /// <returns>Next position as Coordinate if valid colour is added. Current coordinate otherwise.</returns>
        private Coordinate getNextPositionOnMap(Coordinate currentCoordinate, int currentColor)
        {
            //Black left
            if (currentColor == Colors.Black)
                return new Coordinate(currentCoordinate.x - 1, currentCoordinate.y);

            // Blue right
            else if (currentColor == Colors.Blue)
                return new Coordinate(currentCoordinate.x + 1, currentCoordinate.y);

            // Pink down
            else if (currentColor == Colors.Pink)
                return new Coordinate(currentCoordinate.x, currentCoordinate.y + 1);

            // Yellow up
            else if (currentColor == Colors.Yellow)
                return new Coordinate(currentCoordinate.x, currentCoordinate.y - 1);

            // red, exit 
            else
                return currentCoordinate;
        }


        /// <summary>
        /// Gets all agents that are traveling towards a intersection
        /// </summary>
        /// <param name="inter">The intersection to get cars from</param>
        /// <returns>List of agents that traveling to the intersection</returns>
        private List<IAgent> getAgents(Intersection inter) => Cars.Where(x => inter.EntranceCoordinates.Contains((x as Car).Position)).ToList();


        /// <summary>
        /// Runs the simulation locally in the same way as distributed, but without the communication overhead.
        /// SimController must be initialised properly in advance.
        /// </summary>
        public void RunSimulationLocally(int NumberOfFrames)
        {
            //Change the value to fit the right length of the simulation
            int framesToSimulate = NumberOfFrames;

            List<Task> tasks = CreateTasks();
            List<IWorkResult> results = new List<IWorkResult>(); //All results for one frame
            List<IWorkResult> resultsBatch = new List<IWorkResult>(); //Results for a given amount of frames
            StartServer(12345, tasks, framesToSimulate);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            foreach (Task task in tasks)
            {
                (task.Area as CollisionMap).colouredRoad.Convert(BitArray.Conversion.Extract);
                (task.Area as CollisionMap).collisionMap.Convert(BitArray.Conversion.Extract);
            }


            int framesSimulated = 0;
            bool someAgentsExist = true;
            while (someAgentsExist && (framesSimulated < framesToSimulate || framesToSimulate == 0))
            {
                someAgentsExist = false;

                //Calculates the work to be computed for all of the agents
                foreach (Task task in tasks)
                {
                    //Calculates work for each agent for this frame
                    for (int j = 0; j < task.Agents.Count; j++)
                    {
                        IAgent car = task.Agents[j];
                        results.Add(car.CalculateWork(task.Area));

                        //Transfer agents from one task to another if next move is not computable
                        if (car.NextMoveIsComputable){
                            someAgentsExist = true;
                            continue;
                        }
                        //Ignores cars that have reached their destination
                        else if((car as Car).State == CarState.Dead)
                            continue;

                        Border border = (task.Area.Boundaries.First(x => x.Contains(car)) as Border);
                        if (border == null)
                            throw new CarCannotMoveException("No border is matching the position of the agent during expected transfer to other area.");

                        Task taskToTransferTo = tasks.First(x => !x.Equals(task) && x.Area.Boundaries.Any(y => y.Equals(border)));
                        if (taskToTransferTo == null)
                            throw new CarCannotMoveException("No task is matching the border of the position of the agent during expected transfer to other area.");

                        CollisionMap cMap = (taskToTransferTo.Area as CollisionMap);

                        //Checks whether agent can switch area
                        if (!cMap.CoordinateIsTaken(getNextPositionOnMap((car as Car).Position, cMap.GetPixelColouredRoad((car as Car).Position))))
                        {
                            taskToTransferTo.Agents.Add(car);
                            results.Add(taskToTransferTo.Agents[(taskToTransferTo.Agents.Count - 1)].CalculateWork(cMap));
                            (task.Area as CollisionMap).RemoveCar((car as Car).Position);
                            task.Agents.Remove(car);
                            j--; //To compensate from removed car in list
                        }
                        //Agent cannot enter new area, thus agent is in a queue, sorry for the naming
                        else if (car is Car item)
                        {
                            results.Add(new CarSimulationWorkResult(item.Route[0], false, car as Car, (car as Car).Position));
                        }
                        else
                        {
                            throw new NotSupportedException("In this simulation a car must be a Car, car was not a Car");
                        }
                    }
                }

                //Applies the work for all of the agents
                foreach (Task task in tasks)
                    foreach (IAgent agent in task.Agents)
                        agent.ApplyWork(task.Area);

                //Adds timestamp to the result (this is normally done by the server) and adds it to the list containing all the not written results
                foreach (IWorkResult result in results)
                {
                    if (result != null)
                    {
                        result.FrameNumber = framesSimulated;
                        resultsBatch.Add(result);
                    }
                }
                
                results.Clear();
                framesSimulated++;
            }
            _server.DecideSolution(resultsBatch, true);
            sw.Stop();
            File.AppendAllText("Run.csv", sw.Elapsed.TotalMilliseconds.ToString()+"\n");
        }
    }
}