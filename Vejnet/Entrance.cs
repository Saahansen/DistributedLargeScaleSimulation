﻿using System.Drawing;

namespace Vejnet
{
    /// <summary>
    /// This is a spawn-point for cars
    /// </summary>
    [System.Serializable]
    public class Entrance : Vertex
    {
        public Entrance(int x, int y) : base(x, y) { }

        public override string ToString()
            => $"Entrance: {coordinate.x}, {coordinate.y}";
    }
}