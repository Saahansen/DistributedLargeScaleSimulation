using System;
using System.Drawing;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using System.IO;
using Framework;
using System.Net;
namespace Vejnet
{
    public class DisplayMenu
    {
        private string _port = null;
        private string _IPadress = null;
        private string _map = null;
        private int _numberOfFrames = 0;
        private int _numberOfClients = 1;
        public DisplayMenu()
        {
            Console.WriteLine("\n\n\n");
            InitialMenu();
        }
        private void InitialMenu()
        {
            
            int selectionNumber;
            Console.WriteLine("\n \n Press 1 for Client \n Press 2 for Server");
            bool InputIsValid = int.TryParse(Console.ReadLine(), out selectionNumber);
            if (InputIsValid && selectionNumber == 1 || selectionNumber == 2 || selectionNumber == 3)
            {
                if (selectionNumber == 1)
                    ClientMenu();
                else if (selectionNumber == 2)
                    ServerMenu();
                else
                    LocalSimulationMenu();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\n \n----Invalid input---- \n");
                InitialMenu();
            }
        }
        private void ClientMenu()
        {
            int selectionNumber;
            Console.WriteLine((_port == null) ? "Press 1 to specify port." : "Press 1 to edit port. Current port: " + _port);
            Console.WriteLine((_IPadress == null) ? "Press 2 to specify IP adress." : "Press 2 to edit IP adress. Current IP: " + _IPadress);
            Console.WriteLine("Press 3 to go back to previous menu.");
            if (!(_port == null) && !(_IPadress == null))
                Console.WriteLine("Press 4 to start a client with the specified input");
            bool InputIsValid = int.TryParse(Console.ReadLine(), out selectionNumber);
            if (InputIsValid && selectionNumber == 1 || selectionNumber == 2 || selectionNumber == 3 || selectionNumber == 4)
            {
                if (selectionNumber == 1)
                {
                    Console.WriteLine("Enter port:");
                    _port = Console.ReadLine();
                    ClientMenu();
                }
                else if (selectionNumber == 2)
                {
                    Console.WriteLine("Enter Ip adress:");
                    _IPadress = Console.ReadLine();
                    ClientMenu();
                }
                else if (selectionNumber == 3)
                {
                    InitialMenu();
                }
                else if (selectionNumber == 4 && !(_port == null) && !(_IPadress == null))
                {
                    var ip = IPAddress.Parse(_IPadress);
                    var port = ushort.Parse(_port);
                    Client client = new Client(ip, port);
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("----Specify input----");
                    ClientMenu();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("----Invalid input---- \n");
                ClientMenu();
            }
        }
        private void ServerMenu()
        {
            int selectionNumber;
            Console.WriteLine((_port == null) ? "Press 1 to specify port.": "Press 1 to edit port. Current port: " + _port);
            Console.WriteLine((_map == null) ? "Press 2 to specify map." : "Press 2 to edit map. Current map: " + _map);
            Console.WriteLine((_numberOfFrames == 0) ? "Press 3 to specify number of frames to run." : "Press 3 to edit number of frames to run. Current number: " + _numberOfFrames);
            Console.WriteLine("Press 4 to specify number of expected clients. Current number: " + _numberOfClients);
            Console.WriteLine("Press 5 to go back to previous menu.");
            if (!(_port == null) && !(_map == null))
                Console.WriteLine("Press 6 to start a Server with the specified input");
            else
                System.Console.WriteLine("Port and map must be specified to be able to start the simulation");


            bool InputIsValid = int.TryParse(Console.ReadLine(), out selectionNumber);
            if (InputIsValid && selectionNumber >= 1 && selectionNumber <= 6)
            {
                if (selectionNumber == 1)
                {
                    Console.WriteLine("Enter port:");
                    _port = Console.ReadLine();
                    ServerMenu();
                }
                else if (selectionNumber == 2)
                {
                    Console.WriteLine("Enter map:");
                    _map = Console.ReadLine();
                    ServerMenu();
                }
                else if(selectionNumber == 3)
                {
                    Console.WriteLine("Enter number of frames to run:");
                    _numberOfFrames = Int32.Parse(Console.ReadLine());
                    ServerMenu();
                }
                else if(selectionNumber == 4)
                {
                    System.Console.WriteLine("Enter number of expected clients:");
                    _numberOfClients = Int32.Parse(Console.ReadLine());
                    ServerMenu();
                }
                else if (selectionNumber == 5)
                {
                    InitialMenu();
                }
                else if (selectionNumber == 6 && !(_port == null) && !(_map == null))
                {
                    var simControl = new SimulationController(_map);
                    int cars = 1000;
                    simControl.ImportCars(CarLoader.MakeCarsRandomly(cars, simControl.EntranceList, simControl.ExitList, simControl.Graph));
                    List<Task> tasks = simControl.CreateTasks();
                    ushort port = ushort.Parse(_port);
                    simControl.StartServer(port, tasks, _numberOfFrames); //Currently this also starts the sim iteself
                    simControl.StartSimulation(_numberOfClients); //Starts the distributing of workload
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("----Specify input----");
                    ServerMenu();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("----Invalid input---- \n");
                ServerMenu();
            }
        }


        private void LocalSimulationMenu()
        {
            int selectionNumber;
            Console.WriteLine((_map == null) ? "Press 1 to specify map." : "Press 1 to edit map. Current map: " + _map);
            Console.WriteLine("Press 2 to go back to previous menu.");
            if (!(_map == null))
                Console.WriteLine("Press 3 to start a local simulation with the specified input");
            bool InputIsValid = int.TryParse(Console.ReadLine(), out selectionNumber);
            if (InputIsValid && selectionNumber == 1 || selectionNumber == 2 || selectionNumber == 3)
            {
                if (selectionNumber == 1)
                {
                    Console.WriteLine("Enter map: ");
                    _map = Console.ReadLine();
                    LocalSimulationMenu();
                }
                else if (selectionNumber == 2)
                {
                    InitialMenu();
                }
                else 
                {
                    var simControl = new SimulationController(_map);
                    simControl.ImportCars(CarLoader.MakeCarsFromFile("Antenna1_cars.txt", simControl.EntranceList, simControl.ExitList, simControl.Graph));
                    simControl.RunSimulationLocally(_numberOfFrames);
                }
            }
            else
            {
                Console.WriteLine("----Invalid input----");
                LocalSimulationMenu();
            }

        }
    }
}