using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Framework;

namespace Vejnet
{
    /// <summary>
    /// The map where cars are placed on.
    /// </summary>
    [Serializable]
    public class CollisionMap : IArea
    {
        /// <summary>
        /// The underlying coloured road used to determine direction of cars.
        /// </summary>
        /// <value></value>
        public BitArray colouredRoad { get; }

        /// <summary>
        /// The map with roads drawn black and cars are placed on.
        /// </summary>
        /// <value></value>
        public BitArray collisionMap { get; }

        /// <summary>
        /// The offset the has in the horizontal direction.
        /// Used to translate full coordinate to a coordinate on a submap
        /// </summary>
        public int xOffSet;

        /// <summary>
        /// The offset the has in the vertical direction.
        /// Used to translate full coordinate to a coordinate on a submap
        /// </summary>
        public int yOffSet;

        internal Intersection Intersection;

        /// <summary>
        /// List of the borders from submap to other submaps
        /// </summary>
        /// <value></value>
        public List<IBoundary> Boundaries { get; set; }

        /// <summary>
        /// Describes the size of the area
        /// </summary>
        /// <value></value>
        public float Size { get; set; }
        public int ID { get; set; }

        /// <summary>
        /// Generate CollisionMap from BitArray that represents a coloured-in map
        /// </summary>
        /// <param name="input"></param>
        public CollisionMap(BitArray input)
        {
            Boundaries = new List<IBoundary>();
            colouredRoad = new BitArray(input);
            collisionMap = new BitArray(input);
            for (int i = 0; i < input.Height; i++)
                for (int j = 0; j < input.Width; j++)
                {
                    if (collisionMap.GetPixel(j, i) == Colors.White)
                        continue;

                    if (collisionMap.GetPixel(j, i) == Colors.Red || collisionMap.GetPixel(j, i) == Colors.Green)
                        collisionMap.SetPixel(j, i, Colors.White);
                    else
                        collisionMap.SetPixel(j, i, Colors.Black);
                }
        }


        /// <summary>
        /// Generates CollisionMap from a list of pixels and boundaries
        /// </summary>
        /// <param name="pixels">List of tuples that countain a coordinate and a colour in ARGB format</param>
        /// <param name="boundaries">List of boundaries for the map</param>
        public CollisionMap(List<(Coordinate coor, int color)> pixels, List<IBoundary> boundaries)
        {
            Boundaries = boundaries;

            //Find the offset and the size of the map            
            xOffSet = pixels.Min(x => x.coor.x);
            int maxX = pixels.Max(x => x.coor.x);
            yOffSet = pixels.Min(x => x.coor.y);
            int maxY = pixels.Max(x => x.coor.y);

            int width = maxX - xOffSet + 1;
            int height = maxY - yOffSet + 1;

            //Saves the size of the map
            Size = pixels.Count;

            colouredRoad = new BitArray(width, height);
            collisionMap = new BitArray(width, height);

            //Paints both ColouredMap and CollisionMap with the pixels
            foreach ((Coordinate coor, int color) pixelDef in pixels)
            {
                colouredRoad.SetPixel(pixelDef.coor.x - xOffSet, pixelDef.coor.y - yOffSet, pixelDef.color);
                if (pixelDef.color != Colors.Red && pixelDef.color != Colors.Green) //Leaves out entrances and exits on CollisionMap
                    collisionMap.SetPixel(pixelDef.coor.x - xOffSet, pixelDef.coor.y - yOffSet, Colors.Black);
            }
        }


        /// <summary>
        /// Cheks whether a given coordinate is occupied by another car
        /// </summary>
        /// <param name="coor">Corrdinate to be checked</param>
        /// <returns>True if colour of coordinate on collisionmap is not black. False otherwise.</returns>
        public bool CoordinateIsTaken(Coordinate coor)
            => GetPixelCollisionMap(coor) != Colors.Black;


        /// <summary>
        /// Gets pixel from collisionMap
        /// </summary>
        /// <param name="position">The coordinate that is to be checked</param>
        /// <returns>Colour of the pixel in ARGB format</returns>
        public int GetPixelCollisionMap(Coordinate position)
        {
            checkPosition(position); //Checks whether the coordinate is valid
            return collisionMap.GetPixel(position.x - xOffSet, position.y - yOffSet);
        }


        /// <summary>
        /// Sets pixel on CollisionMap
        /// </summary>
        /// <param name="position">Coordinate to be coloured</param>
        /// <param name="color">Colourcode in ARGB format</param>
        public void SetPixelCollisionMap(Coordinate position, int color)
        {
            checkPosition(position);
            collisionMap.SetPixel(position.x - xOffSet, position.y - yOffSet, color);
        }


        /// <summary>
        /// Gets pixel from ColouredMap
        /// </summary>
        /// <param name="position">Coordinate to be checked</param>
        /// <returns>Colourcode of pixel in ARGB format</returns>
        public int GetPixelColouredRoad(Coordinate position)
        {
            checkPosition(position);
            return colouredRoad.GetPixel(position.x - xOffSet, position.y - yOffSet);
        }


        /// <summary>
        /// Paints a given position black on CollisionMap
        /// </summary>
        /// <param name="position">Position to be coloured black</param>
        public void RemoveCar(Coordinate position) => SetPixelCollisionMap(position, Colors.Black);


        /// <summary>
        /// Moves a car on CollisionMap
        /// </summary>
        /// <param name="car">Car to be moved</param>
        /// <param name="coor">Coordinate to move it to</param>
        public void MoveCarOnMap(Car car, Coordinate coor)
        {
            //Car doesnt move
            if (car.Position.Equals(coor))
                return;

            //Paint the previous position black
            //This works because cars keep a pixel between them when driving on normal roads
            if (GetPixelCollisionMap(car.Position) == car.CarColor)
                SetPixelCollisionMap(car.Position, Colors.Black);

            //Color the new position of the car
            SetPixelCollisionMap(coor, car.CarColor);
        }


        /// <summary>
        /// Checks whether a coodinate is valid
        /// It is not if it is out of the map or it is an empty pixel
        /// </summary>
        /// <param name="pos">Coordinate to be checked</param>
        private void checkPosition(Coordinate pos)
        {
            if (colouredRoad.GetPixel(pos.x - xOffSet, pos.y - yOffSet) == 0)
                throw new IndexOutOfRangeException("Something is trying to access: " + pos);
        }


        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            if (obj is CollisionMap collMap)
            {
                return collMap.xOffSet == xOffSet
                && collMap.yOffSet == yOffSet
                && collMap.collisionMap.Height == collisionMap.Height
                && collMap.collisionMap.Width == collisionMap.Width;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return (xOffSet.GetHashCode() * 7) * (yOffSet.GetHashCode() * 5) * (collisionMap.Height.GetHashCode() * 7) * (collisionMap.Width.GetHashCode() * 3);
        }
    }
}