using System;
using Framework;

namespace Vejnet
{
    [Serializable]
    public class CarSimulationWorkResult : IWorkResult
    {
        public int FrameNumber { get; set; }
        public Coordinate StartCoordinate;
        public bool CarCanMove;
        public int CarColor;
        public Coordinate Destination;

        public CarSimulationWorkResult(Coordinate startCoordinate, bool carCanMove, Car currentCar, Coordinate destionation)
        {
            StartCoordinate = startCoordinate;
            CarCanMove = carCanMove;
            CarColor = currentCar.CarColor;
            Destination = destionation;
        }
    }
}