using System.Collections.Generic;
using System;
using System.Drawing;
using System.Linq;

namespace Vejnet
{
    /// <summary>
    /// This is the class that contains a list of all the vertices in the map and the method for generating the shortest path
    /// </summary>
    [Serializable]
    public class Graph
    {
        public List<Vertex> vertexVisited = new List<Vertex>();
        private List<Vertex> closedSet = new List<Vertex>();
        private List<Vertex> openSet = new List<Vertex>();

        /// <summary>
        /// Generates a route from start to destination
        /// Uses A* algorithm to find the shortest path
        /// </summary>
        /// <param name="start">Start of route</param>
        /// <param name="goal">Destination</param>
        /// <returns>List of edges (roads) in sequence. Throws NoPathExistsException if unable to generate route.</returns>
        public List<Coordinate> GetRoute(Entrance start, Exit goal)
        {
            //https://en.wikipedia.org/wiki/A*_search_algorithm
            start.hscore = getHeuristic(start, goal);
            openSet.Add((start));
            start.gscore = 0;

            //While no route has been found
            while (openSet.Count > 0)
            {
                double lowest = double.MaxValue;
                Vertex currentNode = null;

                //Find the vertex in the openset with the lowest heuristic value
                foreach (Vertex node in openSet)
                {
                    if (node.hscore < lowest)
                    {
                        lowest = node.hscore;
                        currentNode = node;
                    }
                }

                //If the destination has been reached   
                if (currentNode.Equals(goal))
                    return reconstructPath(currentNode);

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                foreach (Edge neighbour in currentNode.Edges)
                {
                    //If this vertex has already visited, then skip (the shortest path there has been found)
                    if (closedSet.Contains(neighbour.Destination))
                        continue;

                    //Addes the current distance of the calculated route
                    double tenative = currentNode.gscore + neighbour.Distance;
                    if (!openSet.Contains(neighbour.Destination))
                    {
                        openSet.Add(neighbour.Destination);
                    }
                    //If a shorter route exists from through the neighbour, then skip
                    else if (tenative >= neighbour.Destination.gscore)
                        continue;

                    //Update values as this is currently the best route in progress
                    neighbour.Destination.cameFrom = currentNode;
                    neighbour.Destination.gscore = tenative;
                    neighbour.Destination.hscore = neighbour.Destination.gscore + getHeuristic(neighbour.Destination, goal);

                    if (currentNode is Intersection)
                        neighbour.Destination.hscore += (currentNode as Intersection).GetLength(currentNode, neighbour.Start);
                }
            }

            throw new NoPathExistsException($"No path exists from: ({start}) to ({goal})");
        }


        /// <summary>
        /// Generates the list of a route by walking it in reverse
        /// </summary>
        /// <param name="destination">The destination of the route</param>
        /// <returns>List of edges (roads) in sequence</returns>
        private List<Coordinate> reconstructPath(Vertex destination)
        {
            Vertex current = destination;
            var edges = new List<Edge>();
            while (current.cameFrom != null)
            {
                Edge shortest = null;

                IEnumerable<Edge> list = current.cameFrom.Edges.Where(x => x.Destination.Equals(current));
                foreach (var edge in list)
                {
                    if (shortest == null)
                        shortest = edge;
                    else
                    {
                        if (edge.Distance < shortest.Distance)
                            shortest = edge;
                    }
                }

                edges.Add(shortest);
                current = current.cameFrom;
            }

            openSet.Clear();
            closedSet.Clear();

            edges.Reverse();
            return edges.Select(x => x.Start).ToList();
        }


        /// <summary>
        /// Get the bee-line (fugleflugt) distance from a point to the destination
        /// </summary>
        /// <param name="point">From here</param>
        /// <param name="goal">To here</param>
        /// <returns>Bee-line distance</returns>
        private double getHeuristic(Vertex point, Exit goal)
        {
            double deltaX = Math.Pow((point.coordinate.x - goal.coordinate.x), 2);
            double deltaY = Math.Pow((point.coordinate.y - goal.coordinate.y), 2);
            return Math.Sqrt(deltaX + deltaY);
        }


        /// <summary>
        /// Builds the graph from an entrance
        /// Adds all edges, reachable from an entrance, to the entrance and intersections on the way.
        /// Shlould be called with all entrances
        /// </summary>
        /// <param name="img">ColouredMap to inspect</param>
        /// <param name="entra">Entrance to work from</param>
        public void getEdges(BitArray img, Entrance entra)
        {
            int pixelColour = 0;
            if (((entra.coordinate.x + 1) <= (img.Width - 1)))
            {
                pixelColour = img.GetPixel(entra.coordinate.x + 1, entra.coordinate.y);
                if (!(pixelColour == Colors.White || pixelColour == Colors.Red))
                {
                    Edge newEdge = entra.getEdge(img, new Coordinate(entra.coordinate.x + 1, entra.coordinate.y), 0, this);
                    newEdge.Start = new Coordinate(entra.coordinate.x + 1, entra.coordinate.y);
                    findIntersections(img, entra, newEdge);
                }
            }

            if (((entra.coordinate.x - 1) >= 0))
            {
                pixelColour = img.GetPixel(entra.coordinate.x - 1, entra.coordinate.y);
                if (!(pixelColour == Colors.White || pixelColour == Colors.Red))
                {
                    Edge newEdge = entra.getEdge(img, new Coordinate(entra.coordinate.x - 1, entra.coordinate.y), 0, this);
                    newEdge.Start = new Coordinate(entra.coordinate.x - 1, entra.coordinate.y);
                    findIntersections(img, entra, newEdge);
                }
            }

            if (((entra.coordinate.y + 1) <= (img.Height - 1)))
            {
                pixelColour = img.GetPixel(entra.coordinate.x, entra.coordinate.y + 1);
                if (!(pixelColour == Colors.White || pixelColour == Colors.Red))
                {
                    Edge newEdge = entra.getEdge(img, new Coordinate(entra.coordinate.x, entra.coordinate.y + 1), 0, this);
                    newEdge.Start = new Coordinate(entra.coordinate.x, entra.coordinate.y + 1);
                    findIntersections(img, entra, newEdge);
                }
            }

            if ((entra.coordinate.y - 1) >= 0)
            {
                pixelColour = img.GetPixel(entra.coordinate.x, entra.coordinate.y - 1);
                if (!(pixelColour == Colors.White || pixelColour == Colors.Red))
                {
                    Edge newEdge = entra.getEdge(img, new Coordinate(entra.coordinate.x, entra.coordinate.y - 1), 0, this);
                    newEdge.Start = new Coordinate(entra.coordinate.x, entra.coordinate.y - 1);
                    findIntersections(img, entra, newEdge);
                }
            }
        }


        /// <summary>
        /// Adds all edges, reachable from an intersection, to the intersection.
        /// </summary>
        /// <param name="img">ColouredMap to inspect</param>
        /// <param name="inter">Intersection to work from</param>
        private void getEdges(BitArray img, Intersection inter)
        {
            findOrange(img, inter.IntersectionDefinition, 0);
            inter.ColorIntersectionDefinition();
            vertexVisited.Add(inter);

            foreach (var coor in inter.IntersectionDefinition)
            {
                if (!(coor.Item1.x >= img.Width - 1))
                    if (isExit(img.GetPixel(coor.Item1.x + 1, coor.Item1.y), Colors.Black))
                    {
                        Edge newEdge = inter.getEdge(img, new Coordinate(coor.Item1.x + 1, coor.Item1.y), 0, this);
                        newEdge.Start = new Coordinate(coor.Item1.x + 1, coor.Item1.y);
                        findIntersections(img, inter, newEdge);
                    }

                if (!(coor.Item1.x <= 0))
                    if (isExit(img.GetPixel(coor.Item1.x - 1, coor.Item1.y), Colors.Blue))
                    {
                        Edge newEdge = inter.getEdge(img, new Coordinate(coor.Item1.x - 1, coor.Item1.y), 0, this);
                        newEdge.Start = new Coordinate(coor.Item1.x - 1, coor.Item1.y);
                        findIntersections(img, inter, newEdge);
                    }

                if (!(coor.Item1.y >= img.Height - 1))
                    if (isExit(img.GetPixel(coor.Item1.x, coor.Item1.y + 1), Colors.Yellow))
                    {
                        Edge newEdge = inter.getEdge(img, new Coordinate(coor.Item1.x, coor.Item1.y + 1), 0, this);
                        newEdge.Start = new Coordinate(coor.Item1.x, coor.Item1.y + 1);
                        findIntersections(img, inter, newEdge);
                    }

                if (!(coor.Item1.y <= 0))
                    if (isExit(img.GetPixel(coor.Item1.x, coor.Item1.y - 1), Colors.Pink))
                    {
                        Edge newEdge = inter.getEdge(img, new Coordinate(coor.Item1.x, coor.Item1.y - 1), 0, this);
                        newEdge.Start = new Coordinate(coor.Item1.x, coor.Item1.y - 1);
                        findIntersections(img, inter, newEdge);
                    }
            }
        }


        /// <summary>
        /// Finds the vertex at the end of an edge and calls getEdges on it, if it is an intersection that has not been scanned.
        /// </summary>
        /// <param name="img">ColouredMap to inspect</param>
        /// <param name="entra">Vertex to work from</param>
        /// <param name="newEdge">Edge to walk</param>
        private void findIntersections(BitArray img, Vertex entra, Edge newEdge)
        {
            if (newEdge.Destination is Intersection)
            {
                Vertex savedVertex = vertexVisited.FirstOrDefault(x => x.Equals(newEdge.Destination));
                if (savedVertex == null)
                    getEdges(img, newEdge.Destination as Intersection);
                else
                    newEdge.Destination = savedVertex;

            }
            entra.Edges.Add(newEdge);
        }


        /// <summary>
        /// Finds all the orange connected to a given coordinate containing an orange pixel. 
        /// Adds it to the input list of coordinates.
        /// </summary>
        /// <param name="img">ColouredMap to inspect</param>
        /// <param name="list">List pixels in intersection</param>
        /// <param name="count">Found pixels in intersection</param>
        private void findOrange(BitArray img, List<(Coordinate, int)> list, int count)
        {
            if (list.Count <= count)
                return;

            if (!(list[count].Item1.x >= img.Width - 1))
                if (img.GetPixel(list[count].Item1.x + 1, list[count].Item1.y) == Colors.Orange && list.FirstOrDefault(x => x.Item1.Equals(new Coordinate(list[count].Item1.x + 1, list[count].Item1.y))).Item1 == null)
                    list.Add((new Coordinate(list[count].Item1.x + 1, list[count].Item1.y), 0));

            if (!(list[count].Item1.x <= 0))
                if (img.GetPixel(list[count].Item1.x - 1, list[count].Item1.y) == Colors.Orange && list.FirstOrDefault(x => x.Item1.Equals(new Coordinate(list[count].Item1.x - 1, list[count].Item1.y))).Item1 == null)
                    list.Add((new Coordinate(list[count].Item1.x - 1, list[count].Item1.y), 0));

            if (!(list[count].Item1.y >= img.Height - 1))
                if (img.GetPixel(list[count].Item1.x, list[count].Item1.y + 1) == Colors.Orange && list.FirstOrDefault(x => x.Item1.Equals(new Coordinate(list[count].Item1.x, list[count].Item1.y + 1))).Item1 == null)
                    list.Add((new Coordinate(list[count].Item1.x, list[count].Item1.y + 1), 0));

            if (!(list[count].Item1.y <= 0))
                if (img.GetPixel(list[count].Item1.x, list[count].Item1.y - 1) == Colors.Orange && list.FirstOrDefault(x => x.Item1.Equals(new Coordinate(list[count].Item1.x, list[count].Item1.y - 1))).Item1 == null)
                    list.Add((new Coordinate(list[count].Item1.x, list[count].Item1.y - 1), 0));

            findOrange(img, list, count + 1);
        }


        /// <summary>
        /// Determines wheter a pixel is the first of an exit from an intersection 
        /// </summary>
        /// <param name="inputColor">The color under investigation</param>
        /// <param name="intoColor">The color that would lead back into the intersection</param>
        /// <returns>True if the pixel looked at is not a part of the intersection and not directing into the intersection. False otherwise</returns>
        private bool isExit(int inputColor, int intoColor) => !(inputColor == Colors.Orange || inputColor == Colors.White || inputColor == intoColor);
    }
}