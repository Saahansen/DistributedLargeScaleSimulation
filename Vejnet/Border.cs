using System;
using Framework;


namespace Vejnet
{
    [Serializable]
    public class Border : IBoundary
    {
        Coordinate coor1;
        Coordinate coor2;
        public float Weight { get; set; }
        public int ID { get; set; }
        public int Destination { get; set; }

        /// <summary>
        /// Border between two subareas of the world.
        /// </summary>
        /// <param name="one">Coordinate 1 of border</param>
        /// <param name="two">Coordinate 2 of border</param>
        public Border(Coordinate one, Coordinate two)
        {
            Weight = 1;
            coor1 = one;
            coor2 = two;
        }


        /// <summary>
        /// Check whether a car is within the border.
        /// </summary>
        /// <param name="obj">Car to check whether is in the border.</param>
        /// <returns>True if the car is within the border. False otherwise.</returns>
        public bool Contains(IAgent obj)
        {
            var car = obj as Car;
            return car.Position.Equals(coor1) || car.Position.Equals(coor2);
        }

        /// <summary>
        /// Checks whether two borders are the same
        /// </summary>
        /// <param name="obj">Border to compare to.</param>
        /// <returns>True if both coordinates of both borders are the same. False otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Border))
                return false;

            //Cheks whether the coodinates of the border are the same. The order is irrelevant.
            var border = obj as Border;
            return (border.coor1.Equals(coor1) && border.coor2.Equals(coor2)) || (border.coor1.Equals(coor2) && border.coor2.Equals(coor1));
        }

        public override int GetHashCode()
        {
            return coor1.GetHashCode() * 71 * coor2.GetHashCode();
        }

        public override string ToString() => $"Border: {coor1}, {coor2}";
    }
}

