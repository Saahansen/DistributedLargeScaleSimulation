using System;
using System.Runtime.Serialization;

namespace Vejnet
{
    [Serializable]
    internal class NoPathExistsException : Exception
    {
        public NoPathExistsException()
        {
        }

        public NoPathExistsException(string message) : base(message)
        {
        }

        public NoPathExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoPathExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}