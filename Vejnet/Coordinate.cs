using Framework;

namespace Vejnet
{
    [System.Serializable]
    public class Coordinate
    {
        public int x, y;
        public Coordinate(int xCoordinate, int yCoordinate)
        {
            x = xCoordinate;
            y = yCoordinate;
        }

        /// <summary>
        /// ToString method for coordinate
        /// </summary>
        /// <returns>'Coordinate: x, y'</returns>
        public override string ToString()
        {
            return $"Coordinate: {x}, {y}";
        }

        public override int GetHashCode()
        {
            return (x.GetHashCode() * 7) * (y.GetHashCode() * 5);
        }

        /// <summary>
        /// Compares two coordinates
        /// </summary>
        /// <param name="obj">Coordinate to be compared to</param>
        /// <returns>True if x and y values are the same</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            Coordinate Snew = (Coordinate)obj;
            if (Snew.x == x && Snew.y == y)
                return true;
            else return false;
        }

        /// <summary>
        /// Makes a new coordinate that is NOT reference-equals
        /// </summary>
        /// <returns>New coordinate with the same values</returns>
        public Coordinate Clone()
            => new Coordinate(x, y);
    }
}