using System;

namespace Vejnet
{
    public static class ParseArgs
    {
        public static string Role = "local";
        public static string IP = null;
        public static ushort Port = 12345;
        public static string File = null;
        public static string AgentCreation = "random";
        public static int NumAgents = 100;
        public static int NumFrames = 0;
        public static int NumClients = 0;

        private static String usage =
            @"Usage: dotnet run [options]                                                 
                                                                                        
Options:                                                                    
  h, help       Display this information.                                   
  -r            Specify the role (server, client, local).                  
                Default is local                                                                                

  The following options only applies when role is Client 
  -ip           [Required] Specify IP Address                                    
  -po <port>    Set port to <port>.                  
                  Deafult is 12345                                      

  The following options only applies when role is Server or Local
  -i <num>      [Required for server] Set the number of initial clintes to <num>         
  -m <file>     [Required] Place the input map in <file>                   
  -x <file>     Place the input file in <file> 
                  Default is random agents                                         
  -ag <num>     Set the number of agents to <num>                         
                  Default is 100                                            
  -fr <num>     Set the number of frames to <num>                         
                  Default is all frames                                     
                  ";

        public static void parseArgs(String[] args) 
        {
            if (args.Length == 0)
                PrintAndExit(usage, 4);

            for (int i = 0; i < args.Length; i++) 
            {
                switch (args[i]) 
                {
                    case "h":     // FALL THROUGH
                    case "help": //Help
                        PrintAndExit(usage, 0);
                        break;
                    case "-r": 
                        Role = args[++i].ToLower();
                        break;
                    case "-ip":
                        IP = args[++i];
                        break;
                    case "-po":
                    {
                        ushort port = 12345;
                        Boolean b = ushort.TryParse(args[++i], out port);
                        Port = port;
                        break;
                    }
                    case "-m":
                        File = args[++i];
                        break;
                    case "-x":
                        AgentCreation = args[++i];
                        break;
                    case "-ag":
                        NumAgents = Int32.Parse(args[++i]);
                        break;
                    case "-fr":
                        NumFrames = Int32.Parse(args[++i]);
                        break;
                    case "-i":
                        NumClients = Int32.Parse(args[++i]);
                        break;
                    default:
                        PrintAndExit("Could not recognize the argument " + args[i], 3);
                        break;
                }
            }
        }

        /// <summary>
        /// Prints a message and exits
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="status"></param>
        public static void PrintAndExit(String exitMessage, int status) 
        {
            Console.WriteLine(exitMessage);
            Environment.Exit(status);
        }
    }
}