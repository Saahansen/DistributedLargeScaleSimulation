namespace Vejnet
{
    [System.Serializable]
    public static class DirDef
    {
        public enum Direction
        {
            Left,
            Down,
            Right,
            Up
        }

        /// <summary>
        /// Turns the car 90 degrees to the left of its current direction
        /// </summary>
        /// <param name="dir">The current direction</param>
        /// <returns>Direction 90 degrees counterclockwise</returns>
        public static DirDef.Direction TurnCounterClockWise(DirDef.Direction dir)
        {
            switch (dir)
            {
                case DirDef.Direction.Up:
                    return DirDef.Direction.Left;
                case DirDef.Direction.Down:
                    return DirDef.Direction.Right;
                case DirDef.Direction.Left:
                    return DirDef.Direction.Down;
                default:
                    return DirDef.Direction.Up;
            }
        }


        /// <summary>
        /// Turns the car 90 degrees to the right of its current direction
        /// </summary>
        /// <param name="dir">The current direction</param>
        /// <returns>Direction 90 degrees clockwise</returns>
        public static DirDef.Direction TurnClockWise(DirDef.Direction dir)
            => ReverseDirection(TurnCounterClockWise(dir));


        /// <summary>
        /// Reverses current direction
        /// </summary>
        /// <param name="dir">Current direction</param>
        /// <returns>Reversed direction</returns>
        public static DirDef.Direction ReverseDirection(DirDef.Direction dir)
        {
            switch (dir)
            {
                case DirDef.Direction.Up:
                    return DirDef.Direction.Down;
                case DirDef.Direction.Down:
                    return DirDef.Direction.Up;
                case DirDef.Direction.Left:
                    return DirDef.Direction.Right;
                default:
                    return DirDef.Direction.Left;
            }
        }

    }

}