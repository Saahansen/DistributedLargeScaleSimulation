using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Framework;

namespace Vejnet
{
    [Serializable]
    public enum CarState
    {
        Running,
        NotSpawned,
        Dead
    }

    [Serializable]
    public class Car : IAgent
    {
        public Coordinate Position { get; set; }
        private Coordinate _calculatedNextPosition = new Coordinate(0, 0);
        public int CarColor;
        public List<Coordinate> Route;
        public CarState State { get; set; }
        public bool NextMoveIsComputable { get; private set; }
        private static int _ID = 0;
        public int ID { get; } 


        public int PositionColor;

        /// <summary>
        /// Constructor for car.
        /// </summary>
        /// <param name="start">Start entrance</param>
        /// <param name="goal">Destination</param>
        /// <param name="graph">Graph over the world in which the car acts</param>
        /// <param name="carColor">The colourcode of the car in ARGB format</param>
        public Car(Entrance start, Exit goal, Graph graph, int carColor)
        {
            ID = _ID++;
            State = CarState.Running;
            Route = graph.GetRoute(start, goal);
            CarColor = carColor;

            // Gets starting coordinate
            Position = new Coordinate(start.coordinate.x, start.coordinate.y);
        }

        /// <summary>
        /// Constructor for car. It will be coloured Ferrari red.
        /// </summary>
        /// <param name="start">Start entrance</param>
        /// <param name="goal">Destination</param>
        /// <param name="graph">Graph over the world in which the car acts</param>
        public Car(Entrance start, Exit goal, Graph graph) : this(start, goal, graph, Color.FromArgb(255, 212, 0, 0).ToArgb()) { }


        /// <summary>
        /// Calculating state n+1 for car from state n
        /// </summary>
        /// <param name="area">The world in which the car will act.</param>
        /// <returns>The result from the calculation to apply later</returns>
        public IWorkResult CalculateWork(IArea area)
        {
            if(State == CarState.Dead)
                return null;
                
            try
            {
                CollisionMap collMap = (CollisionMap)area;
                ExtractBitArray(collMap.collisionMap);
                ExtractBitArray(collMap.colouredRoad);

                //Gets the colour of the current position
                PositionColor = collMap.GetPixelColouredRoad(Position);

                //Gets the colour of the wanted position
                Coordinate newPosition = getNextPosition(PositionColor);

                bool coordinateIsFree = false;

                if (newPosition != null)
                    coordinateIsFree = !collMap.CoordinateIsTaken(newPosition);

                //In the case of cars on entrance (about to spawn), they must move immediately so that not all cars move on the same state-change (towers of cars)
                if (PositionColor == Colors.Green && coordinateIsFree)
                {
                    _calculatedNextPosition = newPosition;

                    NextMoveIsComputable = true;
                    ApplyWork(area); //Sorry
                }

                //Car is waiting to enter the map
                else if (PositionColor == Colors.Green && !coordinateIsFree)
                {
                    //Making the car wait on the spawn point
                    NextMoveIsComputable = true;
                    _calculatedNextPosition = Position.Clone();
                    return null; //Returns null so not to be drawn on map
                }

                //If car is in front of exit
                else if (newPosition != null && collMap.GetPixelColouredRoad(newPosition) == Colors.Red)
                {
                    NextMoveIsComputable = true;
                    _calculatedNextPosition = newPosition;
                    return null; //Returns null so not to be drawn on map
                }

                //Car is moving on a regular road
                else if (newPosition != null && collMap.GetPixelColouredRoad(newPosition) != Colors.Orange)
                    if (coordinateIsFree)
                        _calculatedNextPosition = newPosition.Clone();
                    else
                        _calculatedNextPosition = Position.Clone();

                //Car is about to enter intersection
                else if (newPosition != null && collMap.GetPixelColouredRoad(newPosition) == Colors.Orange)
                {
                    if (isPossibleToEnterIntersection(newPosition, PositionColor, collMap))
                        _calculatedNextPosition = newPosition.Clone();
                    else
                        _calculatedNextPosition = Position.Clone();
                }

                // Car is currently driving within an intersection
                else if (PositionColor == Colors.Orange)
                {
                    //Car is next to the exit it wants to take
                    if (ExitIsAvailable(collMap))
                    {
                        //Car cannot take the wanted exit
                        if (collMap.CoordinateIsTaken(Route[1]))
                        {
                            _calculatedNextPosition = collMap.Intersection.GetNextPositionInIntersection(Position);
                            NextMoveIsComputable = true;
                            return new CarSimulationWorkResult(Route[1], false, this, _calculatedNextPosition);
                        }

                        //Car can take the wanted exit
                        else
                        {
                            Route.RemoveAt(0); //Updating route
                            coordinateIsFree = true;
                            _calculatedNextPosition = Route[0].Clone();
                        }
                    }
                    //Car is not next to the exit, so it just drives following the intersection
                    else
                    {
                        _calculatedNextPosition = collMap.Intersection.GetNextPositionInIntersection(Position);
                        coordinateIsFree = true;
                    }
                }

                // Car is somehow stuck
                else
                    throw new CarCannotMoveException($"Car is stuck at {Position}");


                NextMoveIsComputable = true;
                if (!coordinateIsFree)
                    return new CarSimulationWorkResult(Route[0], false, this, _calculatedNextPosition);
                else
                    return new CarSimulationWorkResult(Route[0], true, this, _calculatedNextPosition);

            }
            catch (IndexOutOfRangeException)
            {
                NextMoveIsComputable = false;
                return null;
            }
        }

        private static void ExtractBitArray(BitArray collMap)
        {
            if (collMap.isCompressed)
                collMap.Convert(BitArray.Conversion.Extract);
        }

        /// <summary>
        /// Applies the work from CalculateWork on this car
        /// </summary>
        /// <param name="area">The area wherein the work is to be done</param>
        public void ApplyWork(IArea area)
        {
            CollisionMap cMap = (CollisionMap)area;

            if (!NextMoveIsComputable)
            {
                cMap.RemoveCar(Position);
                return;
            }

            //Remove car if it is about to go onto a red pixel
            if (cMap.GetPixelColouredRoad(_calculatedNextPosition) == Colors.Red)
            {
                State = CarState.Dead;
                NextMoveIsComputable = false;
                cMap.RemoveCar(Position);
            }
            else
            {
                cMap.MoveCarOnMap(this, _calculatedNextPosition);
                Position = _calculatedNextPosition.Clone();
            }
        }

        /// <summary>
        /// Determines whether a car has the space required to enter an intersection
        /// </summary>
        /// <param name="newPosition">The position in the intersection that is wanted to enter</param>
        /// <param name="positionColor">The current colourcode of the road in ARGB format</param>
        /// <param name="cMap">The collsionmap to be acted on</param>
        /// <returns>True if no car is at the pixel one in front and one to the left of the current car. False otherwise.</returns>
        private bool isPossibleToEnterIntersection(Coordinate newPosition, int positionColor, CollisionMap cMap)
        {
            if (positionColor == Colors.Black)//Going left into roundabout
                return !cMap.CoordinateIsTaken(new Coordinate(newPosition.x, newPosition.y + 1));
            else if (positionColor == Colors.Blue)//Going right into roundabout
                return !cMap.CoordinateIsTaken(new Coordinate(newPosition.x, newPosition.y - 1));
            else if (positionColor == Colors.Pink)//Going down into roundabout
                return !cMap.CoordinateIsTaken(new Coordinate(newPosition.x + 1, newPosition.y));
            else//Going up into roundabout
                return !cMap.CoordinateIsTaken(new Coordinate(newPosition.x - 1, newPosition.y));
        }


        /// <summary>
        /// Gets the position that should be moved to based on the position colour
        /// </summary>
        /// <param name="PositionColor">Colourcode of a position in ARGB format</param>
        /// <returns>Coordinate with the next position. Null if car is in intersection</returns>
        private Coordinate getNextPosition(int PositionColor)
        {
            //Black left
            if (PositionColor == Colors.Black)
                return new Coordinate(Position.x - 1, Position.y);

            // Blue right
            else if (PositionColor == Colors.Blue)
                return new Coordinate(Position.x + 1, Position.y);

            // Pink down
            else if (PositionColor == Colors.Pink)
                return new Coordinate(Position.x, Position.y + 1);

            // Yellow up
            else if (PositionColor == Colors.Yellow)
                return new Coordinate(Position.x, Position.y - 1);

            // Green, start on first edge 
            if (PositionColor == Colors.Green)
                return new Coordinate(Route[0].x, Route[0].y);

            //Car is in intersection
            else
                return null;
        }

        /// <summary>
        /// Determines wheter it is possible to exit the roundabout
        /// </summary>
        /// <param name="cMap">The collisionmap to be acted on</param>
        /// <returns>True if at exit coordinate and exit is not currently occupied. False otherwise</returns>
        public bool ExitIsAvailable(CollisionMap cMap)
        {
            Coordinate exitCoordinate = Route[1];

            // Car has an exit available
            if (Position.x == exitCoordinate.x && (Position.y + 1 == exitCoordinate.y || Position.y - 1 == exitCoordinate.y))
                return true;
            else if (Position.y == exitCoordinate.y && (Position.x + 1 == exitCoordinate.x || Position.x - 1 == exitCoordinate.x))
                return true;
            // Car doesnt have an exit available
            else
                return false;
        }

        /// <summary>
        /// Determines whether two cars are the same
        /// </summary>
        /// <param name="obj">Car to be compared</param>
        /// <returns>True if both cars have the same position and colour</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Car))
                return false;
                
            return ((obj as Car).ID == ID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }
    }
}