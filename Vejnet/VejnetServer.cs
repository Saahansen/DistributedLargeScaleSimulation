﻿using Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Drawing;
using System.Linq;
using System.Drawing.Imaging;
namespace Vejnet{
    /// <summary>
    /// Server used to set up network communication and combine IWorkResults
    /// </summary>
    public class VejnetServer : Server
    {
        /// <summary>
        /// The collisionMap without cars, used to draw cars on for each result
        /// </summary>
        private Bitmap _collisionMap;
        private Bitmap _colouredMap;
        private List<(Coordinate coordinate, int queueValue)> _heatValues = new List<(Coordinate, int)>();
        private bool doGifResult = false;
        private bool doHeatmapResult = true;
        private List<CarSimulationWorkResult> resultsToBeProcessed = new List<CarSimulationWorkResult>();


        public VejnetServer(ushort port, List<Task> tasks, CollisionMap CollisionMap, int numberOfFrames) : base(port, tasks, numberOfFrames)
        {
            _collisionMap = CollisionMap.collisionMap.ToBitmap();
            _colouredMap = CollisionMap.colouredRoad.ToBitmap();
        }


        /// <summary>
        /// Computes the results indicated by the input to the program.
        /// TODO: add functionality to chose heatmap and gif from the CLI.
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        public object DecideSolution(List<IWorkResult> results, bool simIsDone)
        {
            List<CarSimulationWorkResult> TempCarSimulationWorkResultList = new List<CarSimulationWorkResult>();

            foreach(IWorkResult WorkResult in results)
            {
                if(WorkResult is CarSimulationWorkResult carSimulationWorkResult)
                {
                    TempCarSimulationWorkResultList.Add(carSimulationWorkResult);
                }
            }

            resultsToBeProcessed.AddRange(TempCarSimulationWorkResultList);

            if(doGifResult)
            {
                if(simIsDone)
                {
                    GenerateGifResult(resultsToBeProcessed);
                }
            }
            if(doHeatmapResult)
            {
                if(simIsDone)
                {
                    GenerateHeatmapResult(resultsToBeProcessed);

                }
            }
            TempCarSimulationWorkResultList.Clear();
            return null;
        }

        /// <summary>
        /// Generates a heatmap from list of results
        /// </summary>
        /// <param name="results">Results to combine</param>
        /// <returns> not important, does not get used. Only for framework </returns>
        public object GenerateHeatmapResult(List<CarSimulationWorkResult> results)
        {
            HeatmapImplementation heatmap = new HeatmapImplementation(_colouredMap);
            
            _heatValues = heatmap.UpdateHeatValues(_heatValues, results);
            
            heatmap.CreateHeatmap(_heatValues);
            return heatmap; 
        }


        protected override object CombineResult(List<IWorkResult> results) => DecideSolution(results, base.SimIsDone);


        /// <summary>
        /// Generates the gif
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>        
        public object GenerateGifResult(List<CarSimulationWorkResult> results){
           results.RemoveAll(x => x == null); //Removes results of cars that for example have reache exits
           if(results.Count == 0)
               return null;

           int startTime = results.Min(x => x.FrameNumber);
           int endTime = results.Max(x => x.FrameNumber);

           List<Image> images = new List<Image>();

           //Adds a series of clean bitmaps corrosponding to the amount of frames of results
           for(int i = startTime; i <= endTime; i++){
               images.Add(new Bitmap(_collisionMap));
           }

           //Plots the results in the frames
           foreach(CarSimulationWorkResult result in results){
               (images[result.FrameNumber-startTime] as Bitmap).SetPixel(result.Destination.x, result.Destination.y, Color.FromArgb(result.CarColor));
           }

           //Adds pictures to gif and saves it to disk
           //Loads existing gif if no result has timestamp = 0 (The simulationg is further along)
           ImageManipulation.CreateGif(images, !results.Exists(x => x.FrameNumber == 0));

           return null;
       }
    }
}