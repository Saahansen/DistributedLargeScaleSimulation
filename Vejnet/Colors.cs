using System;
using System.Drawing;

namespace Vejnet
{
    [Serializable]
    public static class Colors
    {
        public static int Black = Color.FromArgb(255, 0, 0, 0).ToArgb();
        public static int Blue = Color.FromArgb(255, 00, 148, 255).ToArgb();
        public static int Green = Color.FromArgb(255, 76, 255, 0).ToArgb();
        public static int Red = Color.FromArgb(255, 236, 28, 36).ToArgb();
        public static int Pink = Color.FromArgb(255, 239, 18, 253).ToArgb();
        public static int Yellow = Color.FromArgb(255, 253, 225, 18).ToArgb();
        public static int Orange = Color.FromArgb(255, 254, 106, 00).ToArgb();
        public static int White = Color.FromArgb(255, 255, 255, 255).ToArgb();


        /// <summary>
        /// Approxmates colours in bitmap to match colours defined in Colors class and paints these in
        /// </summary>
        /// <param name="image">Bitmap to be coloured correctly</param>
        public static void CleanImage(Bitmap image)
        {
            Size imageSize = image.Size;
            for (int x = 0; x < imageSize.Width; x++)
            {
                for (int y = 0; y < imageSize.Height; y++)
                {
                    int PixelColor = Colors.ApproximateColors(image.GetPixel(x, y).ToArgb());
                    image.SetPixel(x, y, Color.FromArgb(PixelColor));
                }
            }
        }

        /// <summary>
        /// Translates a direction to the colour that represents it
        /// </summary>
        /// <param name="dir">Direction</param>
        /// <returns>Colourcode for direction in ARGB format</returns>
        public static int ColorDirection(DirDef.Direction dir)
        {
            switch (dir)
            {
                case DirDef.Direction.Up:
                    return Yellow;
                case DirDef.Direction.Down:
                    return Pink;
                case DirDef.Direction.Left:
                    return Black;
                default:
                    return Blue;
            }
        }


        /// <summary>
        /// A function to guess the nearest color to the current color that is being looked at. 
        /// </summary>
        /// <param name="v">The color looked at in ARGB format</param>
        /// <returns>The approximated colour in ARGB format</returns>
        public static int ApproximateColors(int v)
        {
            Color input = Color.FromArgb(v);
            int closestColor = 0;
            int count = int.MaxValue;


            if (difference(Color.FromArgb(Black), input) < count)
            {
                closestColor = Black;
                count = difference(Color.FromArgb(Black), input);
            }

            if (difference(Color.FromArgb(Blue), input) < count)
            {
                closestColor = Blue;
                count = difference(Color.FromArgb(Blue), input);
            }

            if (difference(Color.FromArgb(Green), input) < count)
            {
                closestColor = Green;
                count = difference(Color.FromArgb(Green), input);
            }

            if (difference(Color.FromArgb(Red), input) < count)
            {
                closestColor = Red;
                count = difference(Color.FromArgb(Red), input);
            }

            if (difference(Color.FromArgb(Pink), input) < count)
            {
                closestColor = Pink;
                count = difference(Color.FromArgb(Pink), input);
            }

            if (difference(Color.FromArgb(Yellow), input) < count)
            {
                closestColor = Yellow;
                count = difference(Color.FromArgb(Yellow), input);
            }

            if (difference(Color.FromArgb(Orange), input) < count)
            {
                closestColor = Orange;
                count = difference(Color.FromArgb(Orange), input);
            }

            if (difference(Color.FromArgb(White), input) < count)
            {
                closestColor = White;
                count = difference(Color.FromArgb(White), input);
            }

            return closestColor;
        }

        /// <summary>
        /// Determines diffrence between two colours as a value
        /// Bigger value = bigger difference
        /// </summary>
        /// <param name="from">Colour 1</param>
        /// <param name="to">Colour 2</param>
        /// <returns>Sum of cubed difference in red, green, and blue colours</returns>
        private static int difference(Color from, Color to)
            => (int)Math.Pow(from.R - to.R, 2) + (int)Math.Pow(from.G - to.G, 2) + (int)Math.Pow(from.B - to.B, 2);
    }
}