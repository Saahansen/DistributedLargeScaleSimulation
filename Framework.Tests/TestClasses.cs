using System;

namespace Tests
{
    [Serializable]
    public class Car
    {
        int ID;
        string Name;
        double Speed;

        public Car(int id, string name, double speed)
        {
            ID = id;
            Name = name;
            Speed = speed;
        }

        public Car(){}

        public override string ToString()
        {
            return $"{Name}[{ID}] is going {Speed} km/h";
        }
        
        // override object.Equals
        public override bool Equals(object obj)
        {            
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            
            if (obj is Car car)
            {
                if (car.ID == ID && car.Name == Name && car.Speed == Speed)
                    return true;
            }
            
            return false;
        }
        
        // override object.GetHashCode
        public override int GetHashCode()
        {
            return (ID.GetHashCode() * 7) * (Speed.GetHashCode() * 5) * (Name.GetHashCode() * 7);
        }
    }
}