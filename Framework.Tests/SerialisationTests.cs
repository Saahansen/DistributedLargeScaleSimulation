using NUnit.Framework;
using Framework;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System;
using Microsoft.Extensions.Logging;

namespace Tests
{
    public class SerialisationTests
    {
        Serialisation Serialisation;

        [SetUp]
        public void setupLogger()
        {
            Serialisation = new Serialisation();
            Log.SetUp(LogLevel.Critical);
            Serialisation.Logger = Log.Factory.CreateLogger<Serialisation>();
        }


        [Test]
        /// <summary>
        /// Tests whether a random class can be deepcopied
        /// </summary>
        public void DeepCopy1()
        {
            Car car1 = new Car(0, "Speedster", 4.4);
            Car car2 = Serialisation.DeepCopy<Car>(car1);
            Assert.AreEqual(car1, car2);
            Assert.False(car1 == car2); //Chack that the cars are not reference equals
        }


        [Test]
        /// <summary>
        /// Tests whether a list of objects can be deepcopied
        /// </summary>
        public void DeepCopyList()
        {
            List<Car> cars = new List<Car>()
            {
                new Car(0, "John", 2),
                new Car(1, "Car", 5.5),
                new Car(5, "McCar", 10),
            };

            List<Car> carsCopy = Serialisation.DeepCopy<List<Car>>(cars);
            Assert.AreEqual(cars, carsCopy);
            Assert.False(cars == carsCopy); //Chack that the cars are not reference equals
        }


        [Test]
        public void TestLoadFromConnectionObject()
        {
            //Setup
            Socket socket = new Socket(new IPEndPoint(IPAddress.Any, 10000).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            ConnectionObject connObject = new ConnectionObject(socket, NetworkUtilities.CLIENTSIZE);
            connObject.TempResultBuffer = new byte[5] {1,2,3,4,5};
            connObject.TempBufferEnd = 5;
            connObject.HasBytes = true;

            //Local Variables
            byte[] buffer;
            int readableBytes;


            (buffer, readableBytes) = Serialisation.LoadFromConnectionObject(connObject);
            Assert.AreEqual(readableBytes, connObject.TempBufferEnd);
            Assert.AreEqual(buffer, connObject.TempResultBuffer);
        }


        [Test]
        public void TestSaveToConnectionObject()
        {
            Socket socket = new Socket(new IPEndPoint(IPAddress.Any, 10000).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            ConnectionObject connObject = new ConnectionObject(socket, NetworkUtilities.CLIENTSIZE);

            Car car1 = new Car(5, "Jake", 5.5);
            byte[] buffer = Serialisation.Serialise(car1);
            Serialisation.SaveToConnectionObject(connObject, buffer, buffer.Length);

            Assert.AreEqual(connObject.HasBytes, true);
            Assert.AreEqual(connObject.TempBufferEnd, buffer.Length);
        }


        [Test]
        public void TestTryDeserialise()
        {
            //Setup
            Socket socket = new Socket(new IPEndPoint(IPAddress.Any, 10000).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            ConnectionObject connObject = new ConnectionObject(socket, NetworkUtilities.CLIENTSIZE);
            Car car1 = new Car(10, "TestCase", 5.5);
            byte[] buffer = Serialisation.Serialise(car1);

            (List<object> results, long wasteData) = Serialisation.TryDeserialise(buffer, buffer.Length);
            Assert.AreEqual(wasteData, buffer.Length);
            Assert.AreEqual(results[0], car1);
        }


        private int MockNetworkBufferCar(byte[] buffer, int readableDataSize)
        {
            Car car1 = new Car(10, "Cool Car", 5.5);
            byte[] carArray = Serialisation.Serialise(car1);
            Array.Copy(carArray, 0, buffer, readableDataSize, carArray.Length);
            return readableDataSize + carArray.Length;
        }

        private int MockNetworkBufferCarHalf(byte[] buffer, int readableDataSize, int whichHalf)
        {
            Car car1 = new Car(10, "Cool Car", 5.5);
            byte[] carArray = Serialisation.Serialise(car1);

            if (whichHalf == 0)
            {
                Array.Copy(carArray, 0, buffer, readableDataSize, carArray.Length / 2);
                return readableDataSize + carArray.Length / 2;
            }
            else
            {
                Array.Copy(carArray, carArray.Length / 2, buffer, readableDataSize, carArray.Length / 2);
                return readableDataSize + carArray.Length / 2;
            }
        }


        [Test]
        public void TestSimulateDeserialise1Run1Car()
        {
            //Setup
            Socket socket = new Socket(new IPEndPoint(IPAddress.Any, 10000).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            ConnectionObject connObject = new ConnectionObject(socket, NetworkUtilities.CLIENTSIZE);

            //Local variables
            List<object> results;
            byte[] buffer;
            int readableDataSize;
            long wasteData;

            //Run
            (buffer, readableDataSize) = Serialisation.LoadFromConnectionObject(connObject);
            readableDataSize = MockNetworkBufferCar(buffer, readableDataSize);
            (results, wasteData) = Serialisation.TryDeserialise(buffer, readableDataSize);
            (wasteData, readableDataSize) = Serialisation.UpdateBuffer(buffer, wasteData, readableDataSize);
            Serialisation.SaveToConnectionObject(connObject, buffer, readableDataSize);

            //Assert
            Assert.True(results.Count == 1, "Results was: {0}", results.Count);
            Assert.AreEqual(wasteData, readableDataSize);
            Assert.IsInstanceOf(typeof(Car), results[0]);
        }


        [Test]
        public void TestSimulateDeserialise1Run3Car()
        {
            //Setup
            Socket socket = new Socket(new IPEndPoint(IPAddress.Any, 10000).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            ConnectionObject connObject = new ConnectionObject(socket, NetworkUtilities.CLIENTSIZE);

            //Local variables
            List<object> results;
            byte[] buffer;
            int readableDataSize;
            long wasteData;

            //Run
            (buffer, readableDataSize) = Serialisation.LoadFromConnectionObject(connObject);
            readableDataSize = MockNetworkBufferCar(buffer, readableDataSize);
            readableDataSize = MockNetworkBufferCar(buffer, readableDataSize);
            readableDataSize = MockNetworkBufferCar(buffer, readableDataSize);
            (results, wasteData) = Serialisation.TryDeserialise(buffer, readableDataSize);
            (wasteData, readableDataSize) = Serialisation.UpdateBuffer(buffer, wasteData, readableDataSize);
            Serialisation.SaveToConnectionObject(connObject, buffer, readableDataSize);

            //Assert
            Assert.True(results.Count == 3, "Results was: {0}", results.Count);
            Assert.AreEqual(wasteData, readableDataSize);
            Assert.IsInstanceOf(typeof(Car), results[0]);
        }


        [Test]
        public void SimulateDeserialiseFailesThenRunsAgainTest()
        {
            //Setup
            Socket socket = new Socket(new IPEndPoint(IPAddress.Any, 10000).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            ConnectionObject connObject = new ConnectionObject(socket, NetworkUtilities.CLIENTSIZE);

            //Local variables
            List<object> results;
            byte[] buffer;
            int readableDataSize;
            long wasteData;

            //First Run
            (buffer, readableDataSize) = Serialisation.LoadFromConnectionObject(connObject);
            readableDataSize = MockNetworkBufferCarHalf(buffer, readableDataSize, 0);
            (results, wasteData) = Serialisation.TryDeserialise(buffer, readableDataSize);
            (wasteData, readableDataSize) = Serialisation.UpdateBuffer(buffer, wasteData, readableDataSize);
            Serialisation.SaveToConnectionObject(connObject, buffer, readableDataSize);

            //First Assert
            Assert.True(wasteData == 0);
            Assert.True(results.Count == 0);
            Assert.AreEqual(buffer, connObject.TempResultBuffer);
            Assert.True(connObject.TempBufferEnd == readableDataSize);
            Assert.True(connObject.HasBytes == true);

            //Second Run
            (buffer, readableDataSize) = Serialisation.LoadFromConnectionObject(connObject);
            readableDataSize = MockNetworkBufferCarHalf(buffer, readableDataSize, 1);
            (results, wasteData) = Serialisation.TryDeserialise(buffer, readableDataSize);
            (wasteData, readableDataSize) = Serialisation.UpdateBuffer(buffer, wasteData, readableDataSize);
            Serialisation.SaveToConnectionObject(connObject, buffer, readableDataSize);

            //Second Assert
            Assert.True(wasteData == readableDataSize);
            Assert.True(readableDataSize == 0);
            Assert.True(results.Count == 1);
            Assert.True(connObject.TempBufferEnd == 0);
            Assert.True(connObject.HasBytes == false);
        }
    }
}