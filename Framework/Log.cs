using Microsoft.Extensions.Logging;

namespace Framework
{
    public static class Log
    {
        public static ILoggerFactory Factory;

        /// <summary>
        /// Sets up the configuration for the logging
        /// </summary>
        public static void SetUp()
        {
            SetUp(LogLevel.Information);
        }

        /// <summary>
        /// Sets up the configuration for the loggin
        /// </summary>
        /// <param name="level">The level of loggin desired</param>
        public static void SetUp(LogLevel level)
        {
            //Enable logging
            if (!System.IO.Directory.Exists("Logs"))
                System.IO.Directory.CreateDirectory("Logs");


            string timestamp = "Logs/log[" + System.DateTime.Now.ToString("yyyy-MM-dd - HH.mm.ss") + "].log";
            Factory = new LoggerFactory().AddDebug().AddFile(timestamp, level);
        }
    }
}