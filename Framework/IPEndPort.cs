using System.Net;
using System;

namespace Framework
{
    /// <summary>
    /// This is necessary to have a serialisable IPEndPoint
    /// </summary>
    [Serializable]
    public class IPEndPort
    {
        public string IP { get; }
        public ushort Port { get; }
        public IPEndPort(string address, ushort port)
        {
            IP = address;
            Port = port;
        }
    }
}