using System;
using System.Collections.Generic;

namespace Framework
{

    /// <summary>
    /// The representation of an area of the simulation
    /// </summary>
    public interface IArea
    {
        List<IBoundary> Boundaries { get; set; }
        int ID {get;set;}
        float Size { get; set; }

        bool Equals(object obj);
        int GetHashCode();
    }
}
