using System;

namespace Framework
{
    /// <summary>
    /// Boundaries represent the overlap between areas. Agents within
    /// boundaries are typically dublicated on multiple machines.
    /// </summary>
    public interface IBoundary
    {
        bool Contains(IAgent obj);
        bool Equals(object obj);
        int GetHashCode();
        float Weight { get; set; }
        int Destination { get; set; }
        int ID { get; set; }
    }
}