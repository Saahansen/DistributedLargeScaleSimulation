using System;

namespace Framework
{
    /// <summary>
    /// This interface is used to represent the results of
    /// one agent in one frame. The only required field to 
    /// implement is a frame number. The user can implement
    /// whatever they need to represent results.
    /// </summary>
    public interface IWorkResult
    {
        int FrameNumber { get; set; }
    }
}