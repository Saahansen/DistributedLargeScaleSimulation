using System;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using static Framework.NetworkUtilities;
using System.Linq;

namespace Framework
{
    public class ClientTask
    {
        internal readonly object boundaryAckLock = new object();                    

        private static int _staticID = 0;
        private int _id { get; set; }
        public ILogger Logger;
        private Client _client;
        internal DistributableTask DisTask;
        internal List<(IAgent agent, int frame)> TempAgents = new List<(IAgent agent, int frame)>();
        internal int FrameNumber;
        internal bool HasAgents = true;
        internal bool IsDead = false;
        internal HashSet<IAgent> agentsInTask = new HashSet<IAgent>();
        internal HashSet<IAgent> agentQuarentine = new HashSet<IAgent>();


        public ClientTask(DistributableTask disTask, Client parentClient)
        {
            _id = _staticID++;
            _client = parentClient;
            DisTask = disTask;
        }


        public void SendInfoOnAmountOfAgents()
        {
            //Tells the server if the task no longer has any agents
            if (DisTask.Task.Agents.Count < 1 && HasAgents)
            {
                Logger.LogDebug("There are no agents in " + DisTask.Task.Area);
                HasAgents = false;
                SerialiseAndSend(_client.ServerSocket, ClientMessage.HasNoAgents, null);
            }
            //Tells the server that a task has reacquired agents
            else if (DisTask.Task.Agents.Count != 0 && !HasAgents)
            {
                Logger.LogDebug("There are now agents in " + DisTask.Task.Area);
                HasAgents = true;
                SerialiseAndSend(_client.ServerSocket, ClientMessage.HasAgents, null);
            }
        }


        /// <summary>
        /// Does the running of the simulation.
        /// </summary>
        /// <param name="frameNumber"></param>
        /// <returns></returns>
        public void RunSingleFrame()
        {
            Logger.LogTrace("Number of frames: " + FrameNumber + "    Max frames: " + _client.NumberOfFrames);
            if (FrameNumber % 100 == 0)
                Console.WriteLine($"({DateTime.Now.ToString("HH.mm.ss")}): Frame: [{FrameNumber}] - Task: {_id}");

            List<IWorkResult> workResults = CalculateAllAgents(DisTask.Task, FrameNumber);
            ApplyAllAgents(DisTask.Task);

            Syncronise(DisTask, FrameNumber);
            Logger.LogDebug("Task: {0}, Frame: ({1}) COMPELETED", _id, FrameNumber);

            sendResults(_client.ServerSocket, workResults, FrameNumber);
            Logger.LogTrace("Task: {0}, Send results", _id);
        }

        /// <summary>
        /// A method for adding all the temperary agent received from other clients
        /// This is necessary in order to control when the agents are added to the clientTask
        /// It also removes duplicate agents and agents not associated with the current frame
        /// </summary>
        /// <param name="disTask"></param>
        public void AddTempAgents()
        {
            lock (TempAgents)
            {
                foreach (var agentFrame in TempAgents)
                {
                    if (agentFrame.frame > FrameNumber)
                        continue;

                    if (!agentsInTask.Add(agentFrame.agent))
                        continue;

                    agentQuarentine.Add(agentFrame.agent);
                    DisTask.Task.Agents.Add(agentFrame.agent);
                }
                
                TempAgents = TempAgents.Where(x => x.frame > FrameNumber).ToList();
            }
        }


        /// <summary>
        /// Checks and sets the state of a task (Is spinning or not)
        /// </summary>
        /// <param name="disTask"></param>
        public bool TaskIsSpinning()
        {
            bool isSpinning;

            lock (boundaryAckLock)
            {
                isSpinning = false;
                foreach (int boundary in DisTask.BoundaryAcks.Keys.ToList())
                {
                    //If atleast one boundary has less than 1 ACK, keep spinning
                    if (DisTask.BoundaryAcks[boundary] < 1)
                        isSpinning = true;
                }
            }
            return isSpinning;
        }


        /// <summary>
        /// Decrements all the values in the clienttask boundary-ack (Dictionary)
        /// </summary>
        public void DecrementBoundaryAck()
        {
            //Decrements all the boundary-values in boundaryAcks by 1
            lock (boundaryAckLock)
            {
                foreach (int boundary in DisTask.BoundaryAcks.Keys.ToList())
                {
                    DisTask.BoundaryAcks[boundary]--;
                }
                Logger.LogInformation("Task: {0}, Spinlock suspended, running next iteration: [{1}]", _id, DisTask.Task.Area.ToString());
            }
        }


        /// <summary>
        /// Syncronises the agents between areas, and removes agents which cannot more anymore
        /// </summary>
        /// <param name="disTask"></param>
        private void Syncronise(DistributableTask disTask, int nextframe)
        {
            RemoveAgents(disTask);

            Dictionary<IBoundary, List<IAgent>> agentsToSend = FindAgentsToSend(disTask);

            TransferAgents(disTask, agentsToSend, nextframe);

            SendAcks(disTask);
        }

        /// <summary>
        /// /// Sends acknowledgements to all the boundaries of the client
        /// </summary>
        /// <param name="disTask"></param>
        private void SendAcks(DistributableTask disTask)
        {
            foreach (IBoundary boundary in disTask.Task.Area.Boundaries)
            {
                IPEndPort client = disTask.BoundaryToSockets[boundary];

                if (client.IP.ToString().Equals(_client.LocalIpAddress.ToString()))
                {
                    ClientTask cTask = _client.ClientTasks.First(x => x.DisTask.Task.Area.ID.Equals(boundary.Destination));
                    lock (cTask.boundaryAckLock)
                    {
                        cTask.DisTask.BoundaryAcks[boundary.ID]++;
                    }
                }
                else
                {
                    //Catches exception if another client is shut down while this client tries to send ACK
                    try
                    {
                        ConnectionObject neighbour = getClientToSendTo(disTask, boundary);
                        SerialiseAndSend(neighbour, ClientMessage.ACK, boundary.Destination, boundary.ID);
                        Logger.LogTrace("Task: {0}, Sent ACK to [{1}] from boundary: {2}", _id, (neighbour.Socket.RemoteEndPoint as IPEndPoint).Address, boundary.Destination);
                    }
                    catch (SocketException)
                    {
                        foreach (ClientTask ctask in _client.ClientTasks)
                            if (ctask.HasAgents)
                                throw;
                    }
                }
            }
        }

        /// <summary>
        /// Sends list of IWorkResults to reciever
        /// </summary>
        /// <param name="connection">The reciever of the results</param>
        /// <param name="workResults">The list of results to be sent</param>                
        private void sendResults(ConnectionObject connection, List<IWorkResult> workResults, int frameNr)
        {
            if (workResults.Count != 0)
                SerialiseAndSend(connection, ClientMessage.Result, workResults);
            else
            {
                //Catches exception if server shuts down while reporting a result
                try
                {
                    SerialiseAndSend(connection, ClientMessage.EmptyResults, frameNr);
                }
                catch(SocketException)
                {
                    foreach (ClientTask ctask in _client.ClientTasks)
                        if (ctask.HasAgents)
                            throw;
                }
            }
        }

        /// <summary>
        /// Gets a connectionObject based on an IP-address.
        /// Either retreives the connectionObject from a local dictionary based on IP
        /// Or creates a new socket connection based on the specified IP
        /// </summary>
        /// <param name="disTask">The distributeable task which are used to find the right client</param>
        /// <param name="boundary">The boundary on which the connection should be created</param>
        /// <returns></returns>
        private ConnectionObject getClientToSendTo(DistributableTask disTask, IBoundary boundary)
        {
            ConnectionObject connObject;
            IPEndPort connectionSocket = disTask.BoundaryToSockets[boundary];
            IPAddress ip = IPAddress.Parse(connectionSocket.IP);

            lock (_client.IpMapping)
            {
                if (_client.IpMapping.ContainsKey(ip))
                {
                    connObject = _client.IpMapping[ip];
                }
                else
                {
                    //Create a new socket connection for the ip-address, specified by IPEndPort (connectionSocket)
                    IPEndPoint ipe = new IPEndPoint(ip, Client.ClientCommunicationPort);
                    Socket tempSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    connObject = new ConnectionObject(tempSocket, NetworkUtilities.CLIENTSIZE);

                    //Create connection between clients
                    connObject.Socket.Connect(ipe);

                    //Add ip to client mapping in dictionary
                    _client.IpMapping.Add(ip, connObject);
                }
            }

            return connObject;
        }


        /// <summary>
        /// Copies agents from one task to another
        /// Checks the agents and determines whether to add the agent to a local task or send to a neighbour
        /// </summary>
        /// <param name="agentsToSend">The List of agents to be send, as well as the boundaries on which the agents are located</param>
        private void TransferAgents(DistributableTask disTask, Dictionary<IBoundary, List<IAgent>> agentsToSend, int nextframe)
        {
            foreach (IBoundary boundary in agentsToSend.Keys)
            {
                IPEndPort client = disTask.BoundaryToSockets[boundary];
                if (client.IP.ToString().Equals(_client.LocalIpAddress.ToString()))
                    SendAgentsToSelf(agentsToSend, nextframe, boundary);
                else
                {
                    ConnectionObject neighbour = getClientToSendTo(disTask, boundary);
                    SendAgentsViaNetwork(neighbour, agentsToSend, nextframe, boundary);
                }                
                    
            }
        }

        /// <summary>
        /// This method is used to send agents to an other task over the
        /// network. This is done using the method SerialisaAndSend.
        /// </summary>
        /// <param name="neighbour">The connectionObject to send to</param>
        /// <param name="agentsToSend">The agents to send</param>
        /// <param name="nextframe">The frame of the</param>
        /// <param name="boundary">The Boundary that the two tasks share</param>
        private void SendAgentsViaNetwork(ConnectionObject neighbour, Dictionary<IBoundary, List<IAgent>> agentsToSend, int nextframe, IBoundary boundary)
        {
            Logger.LogInformation("{0}, Sending {1} agent(s) to: {2}", this, agentsToSend[boundary].Count, (neighbour.Socket.RemoteEndPoint as IPEndPoint).Address);
            foreach (var agent in agentsToSend[boundary])
            {
                SerialiseAndSend(neighbour, ClientMessage.Agent, agent, nextframe, boundary.Destination);
            }
        }

        /// <summary>
        /// This method is used to send agents from one local ClientTask to another, circumventing the use of Sockets.
        /// </summary>
        /// <param name="agentsToSend">The agents which are to be send to the other ClientTask</param>
        /// <param name="nextframe">The specific frame for which the agent is sent, in order to add the agents based on timeframe</param>
        /// <param name="boundary">The boundary over which the agents will be sent</param>
        private void SendAgentsToSelf(Dictionary<IBoundary, List<IAgent>> agentsToSend, int nextframe, IBoundary boundary)
        {
            ClientTask neighbourClient = _client.ClientTasks.First(x => x.DisTask.Task.Area.ID.Equals(boundary.Destination));
            
            lock (neighbourClient.TempAgents)
            {
                foreach (IAgent agent in agentsToSend[boundary])
                {
                    neighbourClient.TempAgents.Add((Serialisation.DeepCopy(agent), nextframe));

                }
            }
        }


        /// <summary>
        /// Finds the agents to send to the respective neighbours, if they happen to be in a boundary
        /// </summary>
        /// <param name="disTask">The Distributeable Task, where the agents are contained</param>
        /// <returns>A Dictionary with the agents to send, and the boundaries where the agents are located</returns>
        private Dictionary<IBoundary, List<IAgent>> FindAgentsToSend(DistributableTask disTask)
        {
            Dictionary<IBoundary, List<IAgent>> agentsToSend = new Dictionary<IBoundary, List<IAgent>>();

            foreach (IBoundary boundary in disTask.Task.Area.Boundaries)
            {
                foreach (IAgent agent in disTask.Task.Agents)
                {
                    if (!boundary.Contains(agent))
                    {
                        agentQuarentine.Remove(agent);
                        continue;
                    }

                    if ( !agentQuarentine.Add(agent))
                        continue;

                    if (agentsToSend.ContainsKey(boundary))
                        agentsToSend[boundary].Add(agent);
                    else
                        agentsToSend.Add(boundary, new List<IAgent>() { agent });
                }
            }

            return agentsToSend;
        }


        /// <summary>
        /// Removes the agents, which cannot move anymore. This is controlled by "NextMoveIsComputable"
        /// </summary>
        /// <param name="disTask">The Distributeable Task, where the agents are to be removed</param>
        private void RemoveAgents(DistributableTask disTask)
        {
            for (int i = 0; i < disTask.Task.Agents.Count; i++)
            {
                IAgent agent = disTask.Task.Agents[i];
                //If an agent in the new list cannot move, we remove an agent from the original list
                if (!agent.NextMoveIsComputable)
                {
                    disTask.Task.Agents.Remove(agent);
                    agentsInTask.Remove(agent);
                    agentQuarentine.Remove(agent);
                    i--;
                }
            }
        }


        /// <summary>
        /// Runs the applywork method on all the agents in a given task
        /// </summary>
        /// <param name="task">The task, where the agents are located</param>
        private void ApplyAllAgents(Task task)
        {
            foreach (IAgent agent in task.Agents)
            {
                agent.ApplyWork(task.Area);
            }
        }

        /// <summary>
        /// Runs CalculateWork on all agents and disposes results that are null
        /// </summary>
        /// <param name="task">The task where the agents are located</param>
        /// <param name="frameNumber">The current framenumber of the specific task</param>
        /// <param name="workResults"></param>
        /// <returns>List of IWorkResult for the given frame</returns>
        private List<IWorkResult> CalculateAllAgents(Task task, int frameNumber)
        {
            List<IWorkResult> resultList = new List<IWorkResult>();

            foreach (IAgent agent in task.Agents)
            {
                IWorkResult result = agent.CalculateWork(task.Area);

                if (result != null)
                {
                    result.FrameNumber = frameNumber;
                    resultList.Add(result);
                }
            }

            return resultList;
        }

        public override string ToString()
        {
            return $"Task: {_id}";
        }
    }
}