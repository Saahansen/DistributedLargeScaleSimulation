using System;
using System.Collections.Generic;

namespace Framework
{
    /// <summary>
    /// Representation of the agents that operate in a given area.
    /// This is sent to the clients so they can run the simulation.
    /// </summary>
    [Serializable]
    public class Task
    {
        public IArea Area { get; }
        public List<IAgent> Agents { get; }

        public Task(IArea area, List<IAgent> agents)
        {
            Area = area;
            Agents = agents;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Task task)
            {
                if (task.Area.Equals(Area))
                    return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Area.GetHashCode() * 7 + Agents.GetHashCode() * 7;
        }
    }
}