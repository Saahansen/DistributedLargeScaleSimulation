using System;
using System.Collections.Generic;
using System.Linq;
using static Framework.Serialisation;

namespace Framework
{
    public static class GraphPartitioning
    {
        public static List<List<Task>> Partition(List<Task> tasks, int numberOfPartitions)
        {
            //Create the transformed graph
            float[,] transformedGraph = createTransformedGraph(CleanInput(new List<Task>(tasks)));

            //Run the partition and save which partition each of the tasks is in
            int[] part = partition(transformedGraph,numberOfPartitions);

            //Use part to conduct the actual split of the tasks into partitions
            return partitionResult(part,tasks);
        }


        /// <summary>
        /// Creates a transformed graph such that the maximum cut on this graph corresponds to the minimum cut on the original graph.
        /// </summary>
        /// <param name="tasks">The tasks from which the graph is made</param>
        /// <returns></returns>
        private static float[,] createTransformedGraph(List<Task> tasks)
        {
            float[,] graph = new float[tasks.Count,tasks.Count];

            //Normalize the sizes of areas and weights of edges
            float largestSize = 0;
            float largestWeight = 0;

            //Finds the largest size and weight
            foreach(Task task in tasks)
            {
                float size = task.Area.Size;

                if(size > largestSize)
                    largestSize = size;

                for(int i = 0; i < task.Area.Boundaries.Count; i++)
                {
                    float weight = 0;
                    weight += task.Area.Boundaries[i].Weight;

                    //Sums the weight of multiple boundaries between two tasks
                    var boundariesSameDestination = task.Area.Boundaries.Where(x => x.Destination.Equals(task.Area.Boundaries[i].Destination)).ToList();
                    for(int j = 1; j < boundariesSameDestination.Count; j++)
                    {
                        weight += boundariesSameDestination[j].Weight;
                        task.Area.Boundaries.Remove(boundariesSameDestination[j]);
                    }
                        
                    if(weight > largestWeight)
                        largestWeight = weight;
                }
            }

            //Divide each value with the largest value to normalise it
            foreach(Task task in tasks)
            {
                task.Area.Size = 1;//= largestSize;

                foreach(IBoundary boundary in task.Area.Boundaries)
                    boundary.Weight = boundary.Weight/largestWeight+5; //Vodoo constants added to weigh boundaries much heavier than tasks
            }



            float augmentingFactor = findAugmentingFactor(tasks);

            for (int i = 0; i < tasks.Count; i++)
            {
                Task task = tasks[i];
                for (int j = 0; j < tasks.Count; j++)
                {
                    Task otherTask = tasks[j];

                    //When comparing with the node it self 0 is entered into the graph
                    if (task.Equals(otherTask))
                        graph[i,j] = 0;
                    
                    //When the two nodes being compared share an edge, the weight of the edge is subtracted from the augmenting facter (S(v_i) * S(v_j)*AugmentingFactor-W(e_ij))
                    else if(task.Area.Boundaries.Any(x => x.Destination.Equals(otherTask.Area.ID)))
                        graph[i,j] = tasks[i].Area.Size*tasks[j].Area.Size*augmentingFactor - tasks[i].Area.Boundaries.First(x => x.Destination.Equals(tasks[j].Area.ID)).Weight;

                    else
                        //If the vertex i does not have an edge to vertex j, then an edge is created between them with
                        //the cost of S(v_i) * S(v_j)*AugmentingFactor. As all sizes and weights are set to 1, this becomes
                        //just the augmenting factor.                                                
                        graph[i,j] = tasks[i].Area.Size*tasks[j].Area.Size*augmentingFactor;
                }
            }

            return graph;
        }


        /// <summary>
        /// Finding the augmenting factor, which is the heuristic used by the partition algorithm.
        /// </summary>
        /// <param name="tasks">The task from which the graph is made</param>
        /// <returns></returns>
        private static float findAugmentingFactor(List<Task> tasks)
        {
            float max = 0;
            foreach(Task task in tasks)
            {
                float value = 0;
                //Find the weights of the all the boundaries of a task and divides it with 2 times the area size
                foreach(IBoundary boundary in task.Area.Boundaries)
                    value += boundary.Weight / (2*task.Area.Size);

                //Sets a new max value
                if(value > max)
                    max = value;
            }
            return max;
        }



        /// <summary>
        /// This method runs the partition part of the algorithm
        /// </summary>
        /// <param name="graph">The transformed graph</param>
        /// <param name="numberOfPartitions">The amount of partitions</param>
        /// <returns>Returns an array telling which part each of the tasks belong to</returns>
        private static int[] partition(float[,] graph,int numberOfPartitions)
        {
            int numberOfTasks = graph.GetLength(0);
            int[] part = new int[numberOfTasks]; //Part[i] = cluster number of v_i
            float[,] gain = new float[numberOfTasks,numberOfPartitions]; //gain[i][j] = g(v_i,V_j)
            bool[] state = new bool[numberOfTasks]; //false = unused, true = used
            int [,] history = new int[numberOfTasks,2]; // history information on move operations
            float [] temp = new float[numberOfTasks]; // temporary gain for move history            
 
            //Put every node in the same partition
            for(int i = 0; i < numberOfTasks; i++)
                part[i] = 0;

            long iterationVariable = 0;
            while(true)     
            {
                //Set every node as unused
                for(int i = 0; i < numberOfTasks; i++)
                {
                    state[i] = false;

                    //Compute the gain of moving each node to each partition
                    for(int j = 0; j < numberOfPartitions; j++)
                        gain[i,j] = calculateGain(i, getPartitionFromNode(i,part), getPartition(j,part), graph);
                }
                
                // Calculate all of the possible good partitions and save the values for each of them in temp
                for(int i = 0; i < numberOfTasks; i++)
                {
                    //Find the largest gain
                    (int indexOfLargestGain, int indexOfPartition) result = getUnusedNodeLargestGain(state,gain,part);
                    state[result.indexOfLargestGain] = true;
                    history[i,0] = result.indexOfLargestGain;
                    history[i,1] = part[result.indexOfLargestGain]; //Saves the partition from which the node came (needs to be set before part)
                    part[result.indexOfLargestGain] = result.indexOfPartition;
                    temp[i] = gain[result.indexOfLargestGain,result.indexOfPartition];

                    //Recalculate gain, now that a move has been done
                    for(int j = 0; j < numberOfTasks; j++)
                    {
                        if(state[j])
                            continue;
                        
                        //Updates the partition from which a node was removed
                        float weightOfMoving = graph[result.indexOfLargestGain,j];
                        if(part[j] == history[i,1])
                            gain[j,result.indexOfPartition] -= 2*weightOfMoving;

                        //Updates the partition to which the node was moved
                        else if(part[j] == result.indexOfPartition)
                            gain[j,result.indexOfPartition] += 2*weightOfMoving;

                        //Updates all other partitions
                        else
                        {
                            gain[j,history[i,1]] += weightOfMoving;
                            gain[j,result.indexOfPartition] -= weightOfMoving;
                        }
                    }
                }

                //Finding the maximum sum in temp
                for(int i = 1; i < numberOfTasks; i++)
                    temp[i] += temp[i-1];

                //Finding the optimal number of changes to make
                float max = temp.Max();
                int index = temp.ToList().IndexOf(max);


                //Restore the changes done after the optimum, if it is not the optimal solution
                if((max > 0 || index != 0) && iterationVariable++ < 50)
                    for(int i = index+1; i < numberOfTasks; i++)
                        part[history[i,0]] = history[i,1];     
                else
                    return part;
            }
        }


        /// <summary>
        /// Gets the unused node with the largest gain and returns its index, along with the index of the partition it is moved to.
        /// </summary>
        private static (int,int) getUnusedNodeLargestGain(bool[] state, float[,] gain, int[] part)
        {
            int indexOfLargest = 0;
            int indexOfPartition = 0;            
            float largestGain = 0;
            int numberOfTasks = state.Count();
            int numberOfPartitions = gain.GetLength(1);

            for(int i = 0; i < numberOfTasks; i++)
            {
                //Skip if node is used
                if(state[i])
                    continue;
                
                for(int j = 0; j < numberOfPartitions; j++)
                {
                    if(j == part[i])
                        continue;

                    if(gain[i,j] > largestGain)
                    {
                        largestGain = gain[i,j];
                        indexOfLargest = i;
                        indexOfPartition = j;
                    }
                }
            }
            return (indexOfLargest,indexOfPartition);
        }


        /// <summary>
        /// Gets the partition to which a given node belongs
        /// </summary>
        /// <param name="node"></param>
        /// <param name="part"></param>
        /// <returns></returns>
        private static int[] getPartitionFromNode(int node, int[] part)
        {
            List<int> result = new List<int>();
            int nodePartition = part[node];
            int j = 0;

            for(int i=0; i< part.Length; i++)
            {
                if(part[i] == nodePartition)
                {
                    result.Add(i);
                    j++;
                }
            }
            return result.ToArray();
        }


        /// <summary>
        /// Gets the indices of all the nodes in the given partition
        /// </summary>
        /// <param name="partition"></param>
        /// <param name="part"></param>
        /// <returns></returns>
        private static int[] getPartition(int partition, int[] part)
        {
            List<int> result = new List<int>();
            int j = 0;

            for(int i=0; i< part.Length; i++)
            {
                if(part[i] == partition)
                {
                    result.Add(i);
                    j++;
                }
            }
            return result.ToArray();
        }


        /// <summary>
        /// Finds the gain of moving the task from the old partition to the new partition
        /// </summary>
        /// <param name="task"></param>
        /// <param name="oldPartition"></param>
        /// <param name="newPartition"></param>
        /// <param name="graph"></param>
        /// /// <param name="tasks"></param>
        /// <returns></returns>
        private static float calculateGain(int taskIndex, int[] oldPartition, int[] newPartition, float[,] graph)
        {
            if (graph == null)
            {
                throw new ArgumentNullException(nameof(graph));
            }

            float gain = 0;
            float loss = 0;

            //Calculates the gain of removing a vertex from the old partition
            foreach(int taskFromPartition in oldPartition)
            {
                if(taskIndex == taskFromPartition)
                    continue;
                gain += graph[taskIndex,taskFromPartition];                      
            }

            //Calculates the loss of adding a vertex to the new partition
            foreach(int taskFromPartition in newPartition)            
                loss += graph[taskIndex,taskFromPartition];                   

            return gain-loss;                        
        }


        /// <summary>
        /// Changes the part array and the list of tasks, to the final partitioning of tasks
        /// </summary>
        /// <param name="part"></param>, and apply it to the tasks
        /// <param name="tasks"></param>
        /// <returns></returns>
        private static List<List<Task>> partitionResult(int[] part, List<Task> tasks)
        {
            List<List<Task>> partitions = new List<List<Task>>();
            int max = part.Max();

            //Add enough list to support the amount of partitions
            for(int i = 0; i <= max; i++)
                partitions.Add(new List<Task>());

            //Add the tasks to the correct partitions
            for(int i = 0; i < part.Length; i++)
                partitions[part[i]].Add(tasks[i]);   

            return partitions;                            
        }        
    }
}