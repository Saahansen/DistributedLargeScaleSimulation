using System.Net.Sockets;
using System;
using System.Net;

namespace Framework
{

    /// <summary>
    /// The purpose of connection object is to gather all of the information needed
    /// for a given connection to another computer.
    /// </summary>
    public class ConnectionObject
    {
        public Socket Socket;
        public IPEndPoint EndPoint;
        public int TempBufferEnd = 0;
        public bool HasBytes = false;
        public byte[] TempResultBuffer;

        public ConnectionObject(Socket socket, int bufferSize)
        {
            this.Socket = socket;
            Socket.ReceiveTimeout = 10;
            TempResultBuffer = new byte[bufferSize];
        }

        public override int GetHashCode()
        {
            return Socket.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
                return false;
            else
            {
                Socket otherSocket = ((ConnectionObject)obj).Socket;
                return (Socket.RemoteEndPoint as IPEndPoint).Address.ToString().Equals((otherSocket.RemoteEndPoint as IPEndPoint).Address.ToString());
            }
        }
    }
}