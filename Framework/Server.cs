using System;
using System.Reflection;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.ComponentModel;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.Extensions.Logging;
using static Framework.NetworkUtilities;
using Framework;
using System.Diagnostics;

namespace Framework
{
    public class Server
    {
        //Key is the frame number, Value is a list of all IWorkresults for a given key
        private Dictionary<int, List<IWorkResult>> _frameKeeper = new Dictionary<int, List<IWorkResult>>();
        //Keeps track of how many tasks have finished a specific frame
        private Dictionary<int, int> _tasksDonePerFrame = new Dictionary<int, int>();

        private List<ConnectionObject> _clients = new List<ConnectionObject>();
        private List<ConnectionObject> _clientQueue = new List<ConnectionObject>();
        private List<DistributableTask> _distributedTasks = new List<DistributableTask>();
        private Socket _serverSocket;
        private int _globalTimeStamp = 0;
        private Serialisation _serialiser = new Serialisation();
        private ushort _port;
        private bool _acceptClientsThread = true;
        private List<Task> _tasks;
        public ILogger Logger;
        private int _totalNumberOfFrames;
        private int _tasksWithAgents = 0;
        private int _isDoneCounter = 0;
        private bool _shutdown = false;
        private object _shutDownLock = new object();
        public bool SimIsDone { get => SimulationDone() || ReachedTimeStamp(); }

        public Server(ushort port, List<Task> tasks, int numberOfFrames)
        {
            Log.SetUp();
            Logger = Log.Factory.CreateLogger<Server>();
            _serialiser.Logger = Log.Factory.CreateLogger<Serialisation>();
            _port = port;
            _tasks = tasks;
            _totalNumberOfFrames = numberOfFrames;
            _tasksWithAgents = tasks.Count;
        }


        /// <summary>
        /// This method starts the acceptance of clients. Once the number of clients
        /// exceeds the input for the method, the simulation is distributed between
        /// each of the clients. Then the clients run the simulation and report
        /// results back to the server, as they are achieved.
        /// </summary>
        /// <param name="numberOfClients" is the number of clients that need to connect before 
        /// the simulation begins></param>
        public void Start(int numberOfClients)
        {
            //Create a socket to communicate with clients
            _serverSocket = CreateAndStartServerSocket(_port);

            //Wait for clients to connect and create connections to them
            connectToClients(numberOfClients);

            //Add the destination field for each of the boundaries
            addDestinationToBoundaries(_tasks);

            //Maps the different tasks to the different clients
            Dictionary<DistributableTask, ConnectionObject> TaskManager = mapTasksToClients(_tasks);

            //In each distributable task a dictionary is filled indicating the 
            //destination client of each of their boundaries                    
            fillTaskManager(TaskManager, _tasks);

            //Assigns the tasks as they are mapped using the task manager
            assignTasksToClients(TaskManager);

            //Start a thread that can recieve the results reported to the server    
            var resultsThread = new Thread(new ThreadStart(receiveFromClients));
            resultsThread.Start();

            //Start the thread to combine results
            Thread combineFrames = new Thread(new ThreadStart(combineCompleteFrames));
            combineFrames.Start();

            //Send end-of-message to all the clients, to start them working
            sendEndOfMessage(_clients);

            Stopwatch simTimer = new Stopwatch();
            simTimer.Start(); 

            waitForClientsToFinishSimulation(resultsThread);

            combineFrames.Join();

            simTimer.Stop();
            double simTime = simTimer.Elapsed.TotalMilliseconds;
            File.AppendAllText("Run.csv", "Time: " + simTime + " Frames: " + _globalTimeStamp + "\n");

            return;
        }


        /// <summary>
        /// Starts a new thread to accept clients trying to connect the server. It then
        /// spinns until all initial clients have connected. Then the clients from the
        /// clientqueue (filled in acceptClients) are put over _clients.
        /// </summary>
        /// <param name="numberOfClients"></param>
        private void connectToClients(int numberOfClients)
        {
            new Thread(new ThreadStart(acceptClients)).Start();
            while (_clientQueue.Count < numberOfClients) { }
            _clients = new List<ConnectionObject>(_clientQueue);
            _clientQueue.Clear();
        }


        /// <summary>
        /// Adds the destination field in all of the boundaries
        /// /// </summary>
        /// <param name="tasks"></param>
        private void addDestinationToBoundaries(List<Task> tasks)
        {
            //Sets the ID to be able to check if a boundary is equal to an area
            for (int i = 0; i < tasks.Count; i++)
                tasks[i].Area.ID = i;

            int boundaryID = 0;
            foreach (Task task in tasks)
                foreach (IBoundary boundary in task.Area.Boundaries)
                {
                    boundary.ID = boundaryID++;
                    //Find the other area that share the given boundary and add that as destination of the boundary
                    boundary.Destination = tasks.First(x => !x.Equals(task) && x.Area.Boundaries.Any(y => y.Equals(boundary))).Area.ID;
                }
        }


        /// <summary>
        /// Partitions the tasks and maps them to the clients that are currently 
        /// connected (in the _clients list)
        /// </summary>
        /// <param name="tasks"></param>
        private Dictionary<DistributableTask, ConnectionObject> mapTasksToClients(List<Task> tasks)
        {
            Dictionary<DistributableTask, ConnectionObject> TaskManager = new Dictionary<DistributableTask, ConnectionObject>();

            //Runs the partitioning
            List<List<Task>> partitions = GraphPartitioning.Partition(tasks,_clients.Count);
            
            //Distributes the partitions among the clients. Note that the amount of partitions is always smaller than that of the clients.
            for (int i = 0; i < partitions.Count; i++)
            {
                List<Task> partition = (List<Task>)partitions[i];
                foreach (Task task in partition)
                    TaskManager.Add(new DistributableTask(task), _clients[i]);
            }

            return TaskManager;
        }


        /// <summary>
        /// Adds through which boundaries tasks are connected to each other
        /// </summary>
        /// <param name="TaskManager"></param>
        /// <param name="tasks"></param>
        private void fillTaskManager(Dictionary<DistributableTask, ConnectionObject> TaskManager, List<Task> tasks)
        {
            foreach (DistributableTask currentDTask in TaskManager.Keys)
            {
                Dictionary<IBoundary, IPEndPort> BoundaryKeeper = new Dictionary<IBoundary, IPEndPort>();
                Task currentTask = currentDTask.Task;
                foreach (DistributableTask neighbourTask in TaskManager.Keys)
                {
                    if (neighbourTask.Task == currentTask)
                        continue;

                    //Finds all the boundaries whose destinations are pointed to the currentTask    
                    List<IBoundary> boundaryList = currentTask.Area.Boundaries.Where(x => x.Destination.Equals(neighbourTask.Task.Area.ID)).ToList();

                    Logger.LogTrace(currentTask.Area.ToString() + " - " + neighbourTask.Task.Area.ToString() + ": " + boundaryList.Count + " boundaries");
                    ConnectionObject cObj = TaskManager.FirstOrDefault(c => c.Key.Task.Equals(neighbourTask.Task)).Value;


                    foreach (IBoundary boundary in boundaryList)
                    {
                        //Add the boundary to the boundary keeper, along with the client information. 
                        BoundaryKeeper.Add(boundary, GetIPEndPortFromIPEndPoint(cObj.Socket.RemoteEndPoint as IPEndPoint));

                        //Fill BoundaryAcks with the boundary from which acks are required
                        neighbourTask.BoundaryAcks.Add(boundary.ID, 0);
                    }
                }
                currentDTask.BoundaryToSockets = BoundaryKeeper;
            }
        }

        /// <summary>
        /// Converts an IPEndPoint to an IPEndPort
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private IPEndPort GetIPEndPortFromIPEndPoint(IPEndPoint point)
        {
            string address = point.Address.ToString();
            int port = point.Port;
            return new IPEndPort(address, (ushort)port);
        }


        /// <summary>
        /// This method is responsible for assigning the tasks to the clients that they have been mapped to.
        /// </summary>
        /// /// <param name="TaskManager" dictionary over tasks and which client they should be sent to></param>
        private void assignTasksToClients(Dictionary<DistributableTask, ConnectionObject> TaskManager)
        {
            int i = 0;
            foreach (DistributableTask dTask in TaskManager.Keys)
            {
                Console.WriteLine($"Sending task {i} of {_tasks.Count}");
                Assign(dTask, TaskManager[dTask]);
                i++;
            }
        }


        /// <summary>
        /// This method accepts clients connecting to the serverSocket. 
        /// It then creates a sockets to the client, when accepting, and adding the client to a list.
        /// Finally a global time stamp is sent to the client.
        /// </summary>
        private void acceptClients()
        {
            try
            {
                while (_acceptClientsThread)
                {
                    Console.WriteLine("Accepting Clients");
                    Socket client = _serverSocket.Accept();
                    Console.WriteLine("Client accepted");
                    ConnectionObject clientObj = new ConnectionObject(client, NetworkUtilities.SERVERSIZE);
                    _clientQueue.Add(clientObj);

                    //Send global timestamp
                    SerialiseAndSend(clientObj, ServerMessage.TimeStamp, _globalTimeStamp, _totalNumberOfFrames);
                    Console.WriteLine("Global timestamp sent");
                }
            }
            catch (SocketException e)
            {
                if (!_acceptClientsThread)
                    Logger.LogInformation("Client is done with the simulation");
                else
                    Logger.LogDebug(e.Message + e.StackTrace);
            }
        }

        /// <summary>
        /// This method assigns tasks to clients.
        /// </summary>
        /// <param name="task" is the task to be sent></param>
        /// <param name="client" is the client the task is sent to></param>
        protected void Assign(DistributableTask task, ConnectionObject client)
        {
            SerialiseAndSend(client, ServerMessage.Setup, task);
            Logger.LogDebug("Task sent to " + (client.Socket.RemoteEndPoint as IPEndPoint).Address);
        }


        /// <summary>
        /// A method that iterates through all of the connections the client has
        /// and receives and handles the messages that are sent through these.
        /// </summary>
        private void receiveFromClients()
        {
            List<ConnectionObject> clientsToRemove = new List<ConnectionObject>();
            while (!_shutdown)
            {
                //Receives messages from all of the clients
                List<(ConnectionObject, List<Object>)> messages = getMessagesFromReceivedData(clientsToRemove);

                //Handles all of the recieved messages
                foreach ((ConnectionObject client, List<Object> state) message in messages)
                    handleClientMessage(message.client, message.state);

                //Removes clients that are no longer connected to the server
                foreach (ConnectionObject client in clientsToRemove)
                {
                    _clients.Remove(client);
                    Logger.LogTrace("Removed client, remaining clients: " + _clients.Count);
                }

                //Exits the simulation when no more clients are connected. This is the case if 
                // all clients are done with the amount of frames given.
                if (_clients.Count < 1)
                    lock (_shutDownLock)
                    {
                        _shutdown = true;
                    }
            }
        }


        /// <summary>
        /// If the server has a completed frame, which correspond with the current frame the server wants to complete
        /// </summary>
        private void combineCompleteFrames()
        {
            while (!ReachedTimeStamp() && !SimulationDone())
            {
                if (_tasksDonePerFrame == null)
                    continue;

                lock (_tasksDonePerFrame)
                {
                    //Continues if there are no full frames to combine
                    if (!_tasksDonePerFrame.ContainsKey(_globalTimeStamp) || !(_tasksDonePerFrame[_globalTimeStamp] == _tasks.Count))
                        continue;
                }

                if (!_frameKeeper.ContainsKey(_globalTimeStamp))
                    addResultsToFrameKeeper(new List<IWorkResult>(), _globalTimeStamp);

                Console.WriteLine("Frame: "+_globalTimeStamp+" Agents: "+_frameKeeper[_globalTimeStamp].Count);
                Logger.LogDebug("{0}", _frameKeeper[_globalTimeStamp]);
                CombineResult(_frameKeeper[_globalTimeStamp]);

                _frameKeeper[_globalTimeStamp].Clear();

                //If the above code has run, then one full frame from every client has been collected and processed                     
                _globalTimeStamp++;
            }
            CombineResult(_frameKeeper[_globalTimeStamp]);
        }

        private bool SimulationDone()
        {
            return _tasksWithAgents == 0 && 
                _tasksDonePerFrame != null && 
                (_tasksDonePerFrame[_globalTimeStamp] != _tasks.Count || _globalTimeStamp + 1 >= _tasksDonePerFrame.Count) && 
                _tasksDonePerFrame.Keys.Count > 0;
        }

        private bool ReachedTimeStamp()
        {
            return _totalNumberOfFrames != 0 && 
                _totalNumberOfFrames <= _globalTimeStamp;
        }


        /// <summary>
        /// Goes through all of the clients and returns all of their messages
        /// <returns>List of all messages recieved from all clients as tuples</returns>
        /// </summary>
        private List<(ConnectionObject, List<Object>)> getMessagesFromReceivedData(List<ConnectionObject> clientsToRemove)
        {
            List<(ConnectionObject, List<Object>)> messages = new List<(ConnectionObject, List<object>)>();
            foreach (ConnectionObject connection in _clients)
            {
                Logger.LogTrace("Current Connection: {0}", (connection.Socket.RemoteEndPoint as IPEndPoint).Address);
                try
                {
                    Logger.LogDebug("Global time: {0}", _globalTimeStamp);

                    //Deserialise will throw socket exception if the connection to the client is closed
                    List<object> results = _serialiser.Deserialise(connection);

                    if (results == null)
                        continue;

                    Logger.LogDebug("Results: {0}", results);

                    foreach (var result in results)
                    {
                        if (!(result is List<Object> state))
                            continue;

                        if (!(state.Count >= 2))
                            continue;

                        if (state[1] is ClientMessage)
                        {
                            messages.Add((connection, state));
                        }
                    }
                }
                catch (SocketException)
                {
                    Logger.LogInformation("Client disconnected");
                    clientsToRemove.Add(connection);
                }
            }
            Logger.LogDebug("Done with all clients (round robin)");

            return messages;
        }


        /// <summary>
        /// Handles the different types of client messages.
        /// </summary>
        /// <param name="client">The client that message has been recieved from</param>
        /// <param name="state">The object that </param>
        private void handleClientMessage(ConnectionObject client, List<object> state)
        {
            ClientMessage message = (ClientMessage)state[1];
            //If the client is done with the sim (No more frames to simulate)
            if (message.Equals(ClientMessage.IsDone))
            {
                _isDoneCounter++;

                if (_isDoneCounter >= _clients.Count)
                    shutDownClients();
            }
            //The client has reaquired agents(after having none)
            else if (message.Equals(ClientMessage.HasAgents))
            {
                _tasksWithAgents++;
                Logger.LogTrace("There is now: " + _tasksWithAgents + " tasks with agents");
            }
            //The client currently has no agents
            else if (message.Equals(ClientMessage.HasNoAgents))
            {
                _tasksWithAgents--;
                Logger.LogTrace("There is now: " + _tasksWithAgents + " tasks with agents");

                //Shut down all clients if noone has any agents
                if (_tasksWithAgents == 0)
                    shutDownClients();
            }
            //Client has calculated results for one state
            else if (message.Equals(ClientMessage.Result))
            {
                List<IWorkResult> workResults = state[2] as List<IWorkResult>;
                int frameNr = workResults[0].FrameNumber;
                Logger.LogDebug("Entered receive results at frame: {0}", frameNr);

                addResultsToFrameKeeper(workResults, frameNr);
                incrementFrameToTask(frameNr);
            }
            //The client has calculated results for one state but has had no agents to generate IWorkResults
            else if (message.Equals(ClientMessage.EmptyResults))
            {
                int frame = (int)state[2];
                Logger.LogDebug("Entered EmptyList at frame: {0}", frame);
                incrementFrameToTask(frame);
            }
        }

        /// <summary>
        /// Adds all the iWorkresults for a given frame to a dictionary,
        /// where for a given framenumber, it returns all the iworkresults.
        /// </summary>
        /// <param name="workResults"></param>
        /// <param name="frameNr"></param>
        private void addResultsToFrameKeeper(List<IWorkResult> workResults, int frameNr)
        {
            if (_frameKeeper.ContainsKey(frameNr))
                _frameKeeper[frameNr].AddRange(workResults);
            else
                _frameKeeper.Add(frameNr, workResults);
        }

        /// <summary>
        /// Foreach frame, if a task has returned its iworkresults,
        /// then increment the _tasksDonePerFrame value for the given frame
        /// </summary>
        /// <param name="frame"></param>
        private void incrementFrameToTask(int frame)
        {
            lock (_tasksDonePerFrame)
            {
                if (_tasksDonePerFrame.ContainsKey(frame))
                    _tasksDonePerFrame[frame]++;
                else
                    _tasksDonePerFrame.Add(frame, 1);
            }
        }


        /// <summary>
        /// Sends end of message to the clients once the server has given all tasks to all of the clients.
        /// </summary>
        /// <param name="clients"></param>
        /// <param name="binaryFormatter"></param>
        static void sendEndOfMessage(List<ConnectionObject> clients)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            foreach (ConnectionObject client in clients)
            {
                SerialiseAndSend(client, ServerMessage.EndOfMessage, null);
            }
        }


        /// <summary>
        /// Waits for the simulation to be done
        /// </summary>
        /// <param name="resultsThread"></param>
        private void waitForClientsToFinishSimulation(Thread resultsThread)
        {
            resultsThread.Join();
            _acceptClientsThread = false;
        }


        /// <summary>
        /// Shuts down all of the clients and then the server
        /// </summary>
        private void shutDownClients()
        {
            Logger.LogInformation("Shutting down clients");
            foreach (var client in _clients)
                SerialiseAndSend(client, ServerMessage.Shutdown, null);

            lock (_shutDownLock)
            {
                _shutdown = true;
            }
        }


        /// <summary>
        /// Is implimented by the user
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        protected virtual object CombineResult(List<IWorkResult> results)
        {
            throw new NotImplementedException();
        }
    }
}