using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace Framework
{
    /// <summary>
    /// Determines how a message from a client should be interpreted
    /// </summary>
    public enum ClientMessage
    {
        ACK,            //ACK is an acknowledgement, meant to tell a client that a connection has finished its work
        Agent,          //Agent is a messageType for the sending of agents across boundaries
        Result,         //Result is meant to be an Enum value for when the message contains client results
        EmptyResults,   //EmptyResults is a clientCode for the server to know that the client is still running, but has no agents to calculate
        IsDone,     //Is sent when the client is completely done
        HasNoAgents,//Is sent when a task no longer has any agents
        HasAgents,  //Is sent when a task reacquires agents
    }

    /// <summary>
    /// Determines how a message from the server should be interpreted
    /// </summary>
    public enum ServerMessage
    {
        Setup,          //Initial is when the server first creates a connection, and has to send all the tasks to a client
        TimeStamp,      //Timestamp is when the server sends a timestamp
        EndOfMessage,   //EndOfMessage is when the server has finished sending tasks, this can be after an Initial or ReDistribution
        ReDistribution,  //ReDistribution is when the server sends new messages to the existing clients
        Shutdown,       //Shuts down the client. No. Matter. What...
    }

    /// <summary>
    /// Determines which type of connection the message originated from
    /// </summary>
    public enum ConnectionType
    {
        Server,     //Server is when the message was sent from an instance of the software classified as a server
        Client      //Client is when the message was sent from an instance of the software classified as a client
    }

    public static class NetworkUtilities
    {
        /// <summary>
        /// The determined BUFFERSIZE of the deserialisation. Used to syncronise the buffersize of the client-object and deserialisation
        /// </summary>
        public readonly static int SERVERSIZE = 2621440 * 25; //50 Megabyte
        public readonly static int CLIENTSIZE = 1310620; //1 Megabyte

        /// <summary>
        /// A method that creates a stream socket on the local machine, that listens to the specified port using TCP.
        /// </summary>
        /// <param name="port">The port that the socket should listen on.</param>
        internal static Socket CreateAndStartServerSocket(ushort port)
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);
            Socket serverSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            serverSocket.Bind(endPoint);
            serverSocket.Listen(10);
            return serverSocket;
        }


        /// <summary>
        /// Tries to find and establish contact with a server specified by an IP-Address and a Port
        /// </summary>
        /// <param name="ipAddress">The IP-Address of the desired server</param>
        /// <param name="port">The Port of the desired server</param>
        /// <returns>A ClientObject which contains a socket-connection to the server</returns>
        public static ConnectionObject CreateConnectionObjectToServer(IPAddress ipAddress, ushort port)
        {
            IPEndPoint ipe = new IPEndPoint(ipAddress, port);
            Socket serverSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            ConnectionObject server = new ConnectionObject(serverSocket, SERVERSIZE);
            server.EndPoint = ipe;
            return server;
        }


        /// <summary>
        /// Serializes and sends objects from client
        /// </summary>
        /// <param name="neighbour"></param>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        public static void SerialiseAndSend(ConnectionObject neighbour, ClientMessage messageType, params object[] message)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                List<object> objectList = new List<object>() {ConnectionType.Client, messageType};

                if (message != null)
                {
                    foreach(var obj in message)
                        objectList.Add(obj);
                }

                binaryFormatter.Serialize(stream, objectList);

                //Locks the socket, so the send is not interrupted by another thread
                lock (neighbour.Socket)
                {
                    neighbour.Socket.Send(stream.ToArray());
                }
            }
        }


        /// <summary>
        /// Serialises and sends objects from server
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        public static void SerialiseAndSend(ConnectionObject destination, ServerMessage messageType, params object[] message)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                List<object> objectList = new List<object>() {ConnectionType.Server, messageType};

                if (message != null)
                {
                    foreach(var obj in message)
                        objectList.Add(obj);
                }

                binaryFormatter.Serialize(stream, objectList);

                //Locks the socket, so the send is not interrupted by another thread
                lock (destination.Socket)
                {
                    destination.Socket.Send(stream.ToArray());
                }
            }
        }
    }
}