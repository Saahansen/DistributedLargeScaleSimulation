using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Extensions.Logging;

namespace Framework
{
    public class Serialisation
    {
        public ILogger Logger;

        /// <summary>
        /// Deserialises data recieved from the input connection.
        /// If some of the data cannot be deserialised it is saved in the connectionobject and reloaded
        /// next time objects are to be deserialised from the connection.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public List<Object> Deserialise(ConnectionObject connection)
        {
            List<Object> results = null;
            byte[] buffer;
            bool caughtException;
            int readableDataSize;
            long wasteData;

            //Load buffer data from a connectionObject if there is any data to be had.
            (buffer, readableDataSize) = LoadFromConnectionObject(connection);
            Logger.LogTrace("[0], Called Load - ReadableData: {0}", readableDataSize);

            //Load data from the network buffer into the application buffer. Updates ReadableDataSize if successful.
            (readableDataSize, caughtException) = ReadNetworkBuffer(buffer, connection, readableDataSize);
            Logger.LogTrace("[1], Read from Network - New ReadableData: {0}", readableDataSize);

            if (!caughtException)
            {
                //Try to deserialise the data in the application buffer
                (results, wasteData) = TryDeserialise(buffer, readableDataSize);
                Logger.LogTrace("[2], Ran deserialiser - Results: {0}, wasteData: {1}", results, wasteData);

                //Reorders the application buffer, if there is any wasteData (Deserialised data) in the array
                (wasteData, readableDataSize) = UpdateBuffer(buffer, wasteData, readableDataSize);
                Logger.LogTrace("[3], Updated Buffer - wasteData: {0}, ReadableData: {1}", wasteData, readableDataSize);
            }

            //If there is still data in the application buffer, save them to the connectionObject
            SaveToConnectionObject(connection, buffer, readableDataSize);

            //If there are any results to return, return them. Else return Null
            return GetResults(results);
        }


        /// <summary>
        /// A method which tries to deserialise the data in the application buffer. 
        /// </summary>
        /// <param name="buffer">The buffer with the data from the network buffer</param>
        /// <param name="readableDataSize">The amount of data which is deamed readable (actual data)</param>
        /// <returns></returns>
        public (List<object> results, long wasteData) TryDeserialise(byte[] buffer, int readableDataSize)
        {
            List<object> results = new List<object>();
            BinaryFormatter binaryFormat = new BinaryFormatter();
            long wastedata = 0;

            try
            {
                using (MemoryStream stream = new MemoryStream(buffer, 0, readableDataSize))
                {
                    stream.Position = 0;
                    while (stream.Position != stream.Length)
                    {
                        object obj = binaryFormat.Deserialize(stream);
                        results.Add(obj);
                        wastedata = stream.Position;
                    }
                }
            }
            catch (SerializationException e)
            {
                //This exception occurs, when we try to deserialise something that cannot be deserialised.
                //This is saved to the buffer untill more data is received.
                Logger.LogTrace(e, "SerialisationException");
            }
            return (results, wastedata);
        }


        /// <summary>
        /// Saves the application data back into the connectionObject, in order to change to listening to another connection
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="buffer"></param>
        /// <param name="readableDataSize"></param>
        public void SaveToConnectionObject(ConnectionObject connection, byte[] buffer, int readableDataSize)
        {
            //Only if there is data in the application buffer will it be saved. Otherwise the connectionObject buffer is cleared
            if (readableDataSize > 0)
            {
                connection.HasBytes = true;
                connection.TempBufferEnd = readableDataSize;
                Array.Copy(buffer, connection.TempResultBuffer, buffer.Length);
                Logger.LogTrace("[4], Saved the data to ConnectionObject");
            }
            else
            {
                connection.HasBytes = false;
                connection.TempBufferEnd = 0;
                Logger.LogTrace("[4], No data could be saved to ConnectionObject");
            }
        }


        /// <summary>
        /// Removes the data which has already been deserialised, as this is wastedata in a limited array
        /// </summary>
        /// <param name="buffer">The buffer to be reorganised</param>
        /// <param name="wasteData">The amount of data which is deamed to be waste</param>
        /// <param name="readableDataSize">The amount of data, which is where the readable data stops</param>
        public (long wasteData, int readableDataSize) UpdateBuffer(byte[] buffer, long wasteData, int readableDataSize)
        {
            if (wasteData != 0)
            {
                int brokenDataSize = readableDataSize - (int)wasteData;

                //Overrides the first part of the buffer with the data which was not deserialised
                Array.Copy(buffer, wasteData, buffer, 0, brokenDataSize);

                return (0, brokenDataSize);
            }
            else
                return (wasteData, readableDataSize);
        }


        /// <summary>
        /// Returns the results if there are any. Will return Null, if there are no results
        /// </summary>
        /// <param name="results">The List of results if there happens to be any results</param>
        /// <returns>A list of results if there any, Null otherwise</returns>
        public List<Object> GetResults(List<object> results)
        {
            if (results == null || results.Count == 0)
                return null;
            else
                return results;
        }


        /// <summary>
        /// Attempts to read from the network buffer, if this succedes, the data is written into the application buffer
        /// This will fail if there is no data in the network buffer for 50ms (Voodoo parameter), as this will trigger a socketException
        /// </summary>
        /// <param name="buffer">The application buffer, which contains the data to be deserialise</param>
        /// <param name="readableDataSize">The amount of data in the buffer which is actual data, and not just 0's</param>
        /// <param name="connection">The connectionObject, which network buffer is to be read from</param>
        /// <returns>A bool to indicate if there are any data from the network buffer, and an updated ReadableDataSize integer</returns>
        public (int datasize, bool caughtException) ReadNetworkBuffer(byte[] buffer, ConnectionObject connection, int readableDataSize)
        {
            try
            {
                int emptyBufferSize = buffer.Length - readableDataSize;
                int bytesReceived = connection.Socket.Receive(buffer, readableDataSize, emptyBufferSize, SocketFlags.None);
                return (readableDataSize + bytesReceived, false);
            }
            catch (SocketException)
            {
                //No data to read
                return (readableDataSize, true);
            }
        }


        /// <summary>
        /// Loads data from the connectionBuffer if there is any data to be retreived. 
        /// It also sets some integers on the length of the data which is readable
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public (byte[] buffer, int ReadAbleDataSize) LoadFromConnectionObject(ConnectionObject connection)
        {
            if (connection.HasBytes)
                return (connection.TempResultBuffer, connection.TempBufferEnd);
            else
                return (new byte[connection.TempResultBuffer.Length], 0);
        }


        /// <summary>
        /// Generic method for serialising data to a bytestream
        /// </summary>
        /// <param name="obj"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static byte[] Serialise(object obj)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                binaryFormatter.Serialize(stream, obj);
                return stream.ToArray();
            }
        }


        /// <summary>
        /// Generic DeepCopy method, where you specify the object to be deepcopied and it will be serialised and deserialised to remove all references
        /// </summary>
        /// <param name="obj">The object which is to be deepcopied</param>
        /// <typeparam name="T">The type of the object, which is to be deepcopied</typeparam>
        /// <returns>A copy of the object, where all the references have been removed</returns>
        public static T DeepCopy<T>(T obj)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream(Serialise(obj)))
            {
                //Moves the read head to the start of the stream
                stream.Seek(0, SeekOrigin.Begin);

                return (T)binaryFormatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Deepcopies everything received to ensure that there are no reference equals
        /// </summary>
        /// <param name="input"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> CleanInput<T>(List<T> input)
        {
            List<T> cleaned = new List<T>();
            foreach (var item in input)
            {
                var result = DeepCopy<T>(item);
                cleaned.Add(result);
            }

            return cleaned;
        }
    }
}