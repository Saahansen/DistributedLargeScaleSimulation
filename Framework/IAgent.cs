using System;

namespace Framework
{

    /// <summary>
    /// The representation of an agent in the simulation
    /// </summary>
    public interface IAgent
    {
        IWorkResult CalculateWork(IArea area);
        void ApplyWork(IArea area);
        bool NextMoveIsComputable { get; }
        bool Equals(object obj);
        int GetHashCode();
    }
}