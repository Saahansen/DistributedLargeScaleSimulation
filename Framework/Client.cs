using System;
using System.Net;
using System.Linq;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using static Framework.NetworkUtilities;
using System.IO;

namespace Framework
{
    public class Client
    {
        internal int NumberOfFrames { get; private set; }
        internal int ThreadsSimulating;
        internal int TimeStamp { get; private set; }
        internal IPAddress LocalIpAddress { get; private set; }
        internal ConnectionObject ServerSocket { get; private set; }
        internal static ushort ClientCommunicationPort = 11111;
        internal List<DistributableTask> Tasks = new List<DistributableTask>();
        internal Dictionary<IPAddress, ConnectionObject> IpMapping = new Dictionary<IPAddress, ConnectionObject>();
        internal List<ClientTask> ClientTasks = new List<ClientTask>();
        internal readonly object ThreadsSimulatingLock = new object();   

        public ILogger Logger;
        private bool _allTasksReceived = false;
        private ServerMessage _previousServerCommand;
        private Serialisation _serialiser = new Serialisation();
        private List<DistributableTask> _taskBuffer = new List<DistributableTask>();
        private List<ConnectionObject> _connections = new List<ConnectionObject>();
        private List<ConnectionObject> _tempConnection = new List<ConnectionObject>();
        private bool _setupEndOfMessageReceived = false;
        private Socket _clientListeningSocket;
        private int _amountOfTaskThreads = Environment.ProcessorCount * 2;
        private bool _shutdown = false;


        /// <summary>
        /// The constructor for the client, which tries to establish a connection with a server as soon as
        /// the ip-address and server-port is validated.
        /// </summary>
        /// <param name="ipAddress">The server Ip-Address as a string</param>
        /// <param name="port">The server-port as a string</param>
        public Client(IPAddress ipAddress, ushort port)
        {
            Log.SetUp(LogLevel.Critical);
            Logger = Log.Factory.CreateLogger<Client>();
            _serialiser.Logger = Log.Factory.CreateLogger<Serialisation>();
            ServerSocket = CreateConnectionObjectToServer(ipAddress, port);
        }


        /// <summary>
        /// Establishes connection to server and runs all tasks when tasks have been recieved
        /// </summary>
        public void Start()
        {
            Stopwatch prog = new Stopwatch();
            StartServerSocket();

            //Sets the local ip-address as the LocalEndPoint of the server-connection
            LocalIpAddress = (ServerSocket.Socket.LocalEndPoint as IPEndPoint).Address;
            Logger.LogInformation("Establish connection, Local IP: {0}", LocalIpAddress);

            _connections.Add(ServerSocket);

            //Starts a thread which listens to all connections
            ThreadsSimulating = 1;
            new Thread(new ThreadStart(ReceiveFromOpenConnections)).Start();
            new Thread(new ThreadStart(ConnectToClients)).Start();

            while (true)
            {
                if (_allTasksReceived)
                {
                    Tasks = new List<DistributableTask>(_taskBuffer);
                    Logger.LogDebug("Started simulating: " + Tasks.Count + " tasks");
                    ThreadsSimulating = Tasks.Count;
                    _taskBuffer.Clear();
                    Dictionary<int, List<ClientTask>> dict = CreateThreadsForTasks();
                    StartTaskThreads(dict);
                    _allTasksReceived = false;
                    _setupEndOfMessageReceived = true;           
                    prog.Start();
                }

                if (ThreadsSimulating < 1)
                {
                    Logger.LogInformation("Simulation is done");
                    break;
                }
            }

            prog.Stop();
            Console.WriteLine("Total program: {0} sec", prog.Elapsed.TotalSeconds);
            
            string filePath = $"Run.csv";
            string result = (prog.Elapsed.TotalMilliseconds).ToString() + "\n";
            File.AppendAllText(filePath, result);

            //Client is done and tells so to the server
            if (!_shutdown)
                SerialiseAndSend(ServerSocket, ClientMessage.IsDone, null);

            //Spins untill all the other clients also are ready to shut down.
            while (true) 
            {
                if (_shutdown)
                    Environment.Exit(0);
            }
        }

        private void StartTaskThreads(Dictionary<int, List<ClientTask>> taskDictionary)
        {
            //Start threads with lists of tasks
            foreach(var key in taskDictionary.Keys)
            {
                //Starts the ClientTask in a seperate thread to make it multithreaded
                new Thread(new ParameterizedThreadStart(obj => RunTasks(taskDictionary[key]))).Start();
            }
        }


        /// <summary>
        /// Creates a thread foreach distributeable task in the _tasks list.
        /// </summary>
        private Dictionary<int, List<ClientTask>> CreateThreadsForTasks()
        {
            Dictionary<int, List<ClientTask>> tasksForThreads = new Dictionary<int, List<ClientTask>>();
            int i = 0;

            //Seperates the tasks into "_amountOfTaskThreads" amount of lists
            foreach (DistributableTask task in Tasks)
            {
                if (!tasksForThreads.ContainsKey(i))
                    tasksForThreads.Add(i, new List<ClientTask>());

                //Creates a new ClientTask to run the Task
                ClientTask cTask = new ClientTask(task, this);
                ClientTasks.Add(cTask);
                tasksForThreads[i].Add(cTask);
                cTask.Logger = Log.Factory.CreateLogger<ClientTask>();

                i = (i + 1) % _amountOfTaskThreads;
            }

            return tasksForThreads;
        }


        /// <summary>
        /// The method is run by a thread, where the thread will run multiple different tasks.
        /// It does this by iterating through each of the tasks independently
        /// </summary>
        /// <param name="tasks">The list of tasks to be run by the thread</param>
        private void RunTasks(List<ClientTask> tasks)
        {
            //Sets initial framenumber for all the tasks in the list of tasks
            foreach (ClientTask cTask in tasks)
            {
                cTask.FrameNumber = TimeStamp;
                cTask.RunSingleFrame();
            }
                
            while(true)
            {
                int numberOfSpinners = 0;

                //Run One frame foreach task in the list of tasks
                foreach (ClientTask cTask in tasks)
                {
                    if (cTask.TaskIsSpinning())
                    {
                        numberOfSpinners++;
                        continue;
                    }

                    cTask.AddTempAgents();
                    cTask.FrameNumber++;
                    cTask.DecrementBoundaryAck();
                    cTask.RunSingleFrame();
                    CheckIfTaskIsDead(cTask);

                    if (NumberOfFrames != 0)
                        continue;

                    //Send information to the server about the state of agents for each task in the list of tasks 
                    cTask.SendInfoOnAmountOfAgents();
                }

                RemoveDeadTasks(tasks);

                //If every task on the thread is spinning, yield the thread
                if (numberOfSpinners == tasks.Count)
                    Thread.Yield();
            }
        }

        private void RemoveDeadTasks(List<ClientTask> tasks)
        {
            for (int taskIndex = 0; taskIndex < tasks.Count; taskIndex++)
            {
                if (tasks[taskIndex].IsDead)
                {
                    tasks.Remove(tasks[taskIndex]);
                    taskIndex--;
                }
            }
        }

        private void CheckIfTaskIsDead(ClientTask cTask)
        {
            if (NumberOfFrames != 0 && cTask.FrameNumber == NumberOfFrames && !cTask.IsDead)
            {
                lock (ThreadsSimulatingLock)
                {
                    ThreadsSimulating--;
                }
                cTask.IsDead = true;
            }
        }


        /// <summary>
        /// Continuously tries to establish a connection to a server. 
        /// Stops, if a connection to the server is established
        /// </summary>
        private void StartServerSocket()
        {
            Console.WriteLine("Looking for server");
            //ServerSocket.Socket.Blocking = false;
            while (true)
            {
                try
                {
                    ServerSocket.Socket.Connect(ServerSocket.EndPoint);
                    Logger.LogInformation("Created Connection to server {0}", (ServerSocket.Socket.RemoteEndPoint as IPEndPoint).Address);
                    break;
                }
                catch (SocketException)
                {
                    Console.WriteLine("Could not find server, sleeping for 1 second");
                    Thread.Sleep(1000);
                }
            }
            //ServerSocket.Socket.Blocking = true;
        }


        /// <summary>
        /// Listens to the ClientCommunicationPort, and accecpt all connection from neighbours trying to connect.
        /// The Accept call is Blocking and it is adviced to run in a seperate thread from the main thread.
        /// </summary>
        private void ConnectToClients()
        {
            IPEndPoint ipe = new IPEndPoint(IPAddress.Any, ClientCommunicationPort);
            _clientListeningSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _clientListeningSocket.Bind(ipe);
            _clientListeningSocket.Listen(5);

            while (true)
            {
                Socket neighbour = _clientListeningSocket.Accept();
                ConnectionObject neighbourObject = new ConnectionObject(neighbour, NetworkUtilities.CLIENTSIZE);

                Logger.LogInformation("Accepted connection from: {0}", (neighbourObject.Socket.RemoteEndPoint as IPEndPoint).Address);

                //Adds new connection to a temporary buffer, before being added to the list of reading connection
                lock (_tempConnection)
                {
                    _tempConnection.Add(neighbourObject);
                }
            }
        }


        /// <summary>
        /// Method for receiving from any connection to this particular client
        /// Can be either server or Clients. Spinlocks until any tasks are received by server
        /// </summary>
        private void ReceiveFromOpenConnections()
        {
            while (true)
            {
                if (_setupEndOfMessageReceived)
                {
                    lock (_tempConnection)
                    {
                        //Update connections when all clients are ready to run
                        _connections.AddRange(_tempConnection);
                        _tempConnection.Clear();
                    }
                }

                //Stop communication between clients when simulation is done
                if (ThreadsSimulating < 1)
                {
                    _connections.Clear();
                    _connections.Add(ServerSocket);
                }

                //Go through all current connections and listen to each. (Round Robin approach)
                foreach (ConnectionObject connection in _connections)
                {
                    Logger.LogDebug("Current connection: [{0}]", (connection.Socket.RemoteEndPoint as IPEndPoint).Address);
                    List<Object> receivedObjects = _serialiser.Deserialise(connection);


                    //If receivedObjects is null, then the client has not received any data. Continue listening
                    if (receivedObjects == null || receivedObjects.Count < 1)
                        continue;

                    Logger.LogTrace("ReceivedObjects: {0}", receivedObjects);
                    foreach (List<Object> messageList in receivedObjects)
                    {
                        Logger.LogTrace("The lists are: {0}", messageList);

                        //The connectionType of the message is checked and an appropriate messageHandler is selected
                        if (!(messageList[0] is ConnectionType))
                            continue;

                        ConnectionType conType = (ConnectionType)messageList[0];

                        if (conType.Equals(ConnectionType.Server))
                            HandleServerMessage(messageList);
                        else
                            HandleClientMessage(messageList);
                    }
                }
            }
        }


        /// <summary>
        /// The function handles the messages which are sent from the server. 
        /// The messages are sent as a list of objects, where the first object is 
        /// whether the message came from the server
        /// </summary>
        /// <param name="messageList">The list of objects sent from the server</param>
        private void HandleServerMessage(List<object> messageList)
        {
            if (!(messageList[1] is ServerMessage))
                return;

            ServerMessage servMessage = (ServerMessage)messageList[1];

            Console.WriteLine($"ServerMessage: " + (ServerMessage)servMessage);

            //If the first element (outside ServerCheckBoolean) is an integer, 
            //then the message is interpreted as a timestamp
            if (servMessage.Equals(ServerMessage.TimeStamp) && messageList[2] is int timeStamp && messageList[3] is int frameNr)
            {
                TimeStamp = timeStamp;
                NumberOfFrames = frameNr;
                Logger.LogTrace("Client recieved timestamp: " + TimeStamp + "    Client needs to run: " + NumberOfFrames + " frames");
            }
            //If the element is a distributeable task, then the message is interpreted as work
            else if (servMessage.Equals(ServerMessage.Setup) && messageList[2] is DistributableTask distributeableTask)
            {
                Logger.LogDebug("Task received: {0}", distributeableTask);
                _taskBuffer.Add(distributeableTask);
            }
            else if (servMessage.Equals(ServerMessage.EndOfMessage))
            {
                if (_previousServerCommand.Equals(ServerMessage.Setup))
                    _allTasksReceived = true;
                else if (_previousServerCommand.Equals(ServerMessage.ReDistribution))
                    throw new NotImplementedException();
            }
            else if (servMessage.Equals(ServerMessage.Shutdown))
            {
                Logger.LogInformation("Client was shut down from the server");
                _shutdown = true;
                ThreadsSimulating = 0;
            }
            else if (servMessage.Equals(ServerMessage.ReDistribution)) { throw new NotImplementedException(); }

            _previousServerCommand = servMessage;
        }


        /// <summary>
        /// Handles the message if it is recieved from a client
        /// </summary>
        /// <param name="messageList"></param>
        private void HandleClientMessage(List<object> messageList)
        {
            if (!(messageList[1] is ClientMessage))
                return;

            ClientMessage cMessage = (ClientMessage)messageList[1];

            if (cMessage.Equals(ClientMessage.ACK))
            {
                int boundaryDst = (int)messageList[2];
                int boundaryID = (int)messageList[3];
                Logger.LogDebug("Received ACK: [{0}]", boundaryDst);

                ClientTask cTask = ClientTasks.First(x => x.DisTask.Task.Area.ID.Equals(boundaryDst));
                DistributableTask disTask = cTask.DisTask;

                lock (cTask.boundaryAckLock)
                {
                    disTask.BoundaryAcks[boundaryID]++;
                    Logger.LogTrace("BoundaryAcks[{0}]: {1}", boundaryDst, disTask.BoundaryAcks[boundaryID]);
                }
            }
            else if (cMessage.Equals(ClientMessage.Agent))
            {
                var agent = messageList[2] as IAgent;
                int framenr = (int)messageList[3];
                int boundaryDst = (int)messageList[4];
                Logger.LogInformation("Recieved Agent {0}", messageList[2]);

                ClientTask ctask = ClientTasks.First(x => x.DisTask.Task.Area.ID.Equals(boundaryDst));
                lock (ctask.TempAgents)
                {
                    ctask.TempAgents.Add((agent, framenr));
                }
            }
        }
    }
}
