using System;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Net;

namespace Framework
{
    /// <summary>
    /// Contains a tasks and the relevant information to connect to the machines
    /// containing the tasks with neighbouring area.
    /// </summary>
    [Serializable]
    public class DistributableTask
    {
        public Dictionary<IBoundary, IPEndPort> BoundaryToSockets;
        public Task Task;
        public Dictionary<int, int> BoundaryAcks = new Dictionary<int, int>();

        public DistributableTask(Task task)
        {
            this.Task = task;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is DistributableTask dTask)
            {
                if (Task.Equals(dTask.Task))
                    return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Task.GetHashCode();
        }
    }
}