import os
import sys
import time
import smtplib
import subprocess
from pathlib import Path

MAILFROM = 'applicationds317@gmail.com'
MAILTO = 'applicationds317@gmail.com'
File_Lines = []

isServer = False
isClient = False

def LoadConfig(file):
    global isServer
    global isClient

    file = open(file, "r")
    for string in file.readlines():
        if "-r Server" in string or "-r server" in string:
            isServer = True
        elif "-r Client" in string or "-r client" in string:
            isClient = True
        if string.strip():
            File_Lines.append(string)


def FindProject():
    current_file_path = Path(os.path.dirname(os.path.abspath(__file__)))
    current_file_path = current_file_path.parent
    return next(current_file_path.glob("Vejnet"))


def getTextFromFile(filePath):
    try:
        with open(filePath, "r") as file:
            return file.read()
    except FileNotFoundError:
        return "File not found"


def SendEmail(string):
    if string:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login("applicationds317@gmail.com", "@pplicationDs317")
        message = "Subject: [D502E18], " + string[1:] + " - Completed\n" + string
        message += getTextFromFile("Run.csv")
        server.sendmail(MAILFROM, MAILTO, message)
        server.quit()


def Test(run_args, index):
    print('Starting the "Run" process, nr: {0}'.format(index))
    print('Running with "dotnet run {0}"'.format(run_args.strip()))
    project_folder = FindProject()
    process_result = subprocess.run(["dotnet run -p " + str(project_folder) + " " + run_args], shell=True)
    if(isServer):
        SendEmail("#Tempoary run: " + run_args)
    return process_result.returncode == 0


def RunTests():
    runs = 1
    mailMessage = ""
    for runIndex in range(0, len(File_Lines)):
        if '#' not in File_Lines[runIndex]:
            if(isClient and runs > 1):
                print("Sleeping for two minutes to wait for server")
                time.sleep(300)
            result = Test(File_Lines[runIndex], runs)
            if result:
                print("The run passed and died succesfully, the next run will commence\n")
            else:
                print("The run failed, the next run will commence\n")
            runs += 1
        else:
            SendEmail(mailMessage)
            mailMessage = File_Lines[runIndex]
            print("Added a new line for next batch of runs")
            with open("Run.csv", "a") as file:
                file.write("\n")
    SendEmail(mailMessage)
    print("The Tests have ended")


def Run(file):
    LoadConfig(file)
    RunTests()


if __name__ == "__main__":
    Run(sys.argv[1])
